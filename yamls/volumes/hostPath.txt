#create pod using hostPath volume
#first on openshift, by default the restricted SCC does not allow the deployment of pods that want a hostPath volume. 
#to change that, export the restricted scc, edit it and import it under the name 'restricted-hostpath'
#oc get scc restricted > /tmp/scc
#remove the values under the groups: # we dont want system:auth to all use this scc, therefor we need to remove it.
#change the metadata.name to restricted-hostpath
#edit the metadata description, use anything that satisfies you
#set allowHostDirVolumePlugin to true #allowHostDirVolumePlugin: true
#add value to volumes list, - hostPath #if you do this but forget the step above, it will be removed after saving. 
#create sa and assign the scc to that sa
oc create sa sa-hostpath
oc adm policy add-scc-to-user restricted-hostpath -z sa-hostpath

#patch your dc / use the serviceaccount
#oc set serviceaccount dc <name of deployment> <name of service account>
#or just edit the dc directly oc edit dc ..

#add hostPath volume to host
oc set volume dc/ubuntu-run --add --type hostPath --path /mnt/hostpath --mount-path /test
#to check
oc set volumes --list dc ubuntu-run

#most likely pod must run under privileged scc, still trying to make it work without that
https://docs.openshift.com/container-platform/4.2/storage/persistent-storage/persistent-storage-hostpath.html

#troubleshoot selinux
sealert -a /var/log/audit/audit.log
Additional Information:
Source Context                system_u:system_r:container_t:s0:c9,c27
Target Context                system_u:object_r:mnt_t:s0
Target Objects                x [ file ]
Source                        touch
#so need to figure this out later..