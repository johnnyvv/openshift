# Securing services #
For testing the usage of auto generated certs by annotating the service and let a client consume the CA.

## Way it works ##

A service can be created with an annotation or later on annoted with:
```
oc annotate service <service-name> service.beta.openshift.io/serving-cert-secret-name=<secret-name>
```
OR
Edit the service and place this line under metadata.annotations:
```
      service.alpha.openshift.io/serving-cert-secret-name: <name of secret>
```
For secret name, please use the name of the service. 

After the annotation a secret will be created with the given name. The contents of the secret:
* tls.key - private key
* tls.crt - public key + CA (chain)

## Create configmap and generate CA injection ##

The generated CA can be used by clients to mount the ca and use it in their calls to the endpoint
To generate the configmap and inject the CA in to it, do the following:
```
oc create cm nginx-test-ca
oc annotate configmap nginx-test-ca service.beta.openshift.io/inject-cabundle=true
```

### The generated key pair has the CN like ###

* <service name>.<project>.svc

### The following SAN / Alternative names are added ###

* <service name>.<project>.svc
* <service name>.<project>.svc.cluster.local


## busy box ##
The client pod is using busybox, but that does not support tls verification, so it's kind of useless since all calls to a https endpoint will succeed. 
Therefor, to do is to get another image and use that!

## nginx image ##
If you dont have an nginx image that include the config from /etc/nginx/config.d build it using the Dockerfile in this directory.

the build it within Openshift and deploy it to internal registry ( or external whatever you want):
```
oc new-build --name nginx-ocp --binary=true
oc start-build nginx-ocp --from-dir=.
```
After that, change the image in the file nginx-pod.yaml

## Testing ##
Inside a pod, using curl:
```
  curl https://nginx-test.<project>.svc:443  --cacert /etc/ssl/ocp/ca.crt
```

## To do ##

* client small client image that does have tls verification
* rotating certs

