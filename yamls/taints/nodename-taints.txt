#weird thing with nodeName and taints
steps I took

deploy a pod with tolerations for node-role.kubernetes.io/master:NoSchedule (exists)
    - this results in that the pod gets scheduled on a ordinary worker node. 
apiVersion: v1
kind: Pod
metadata:
  name: ubuntu-tol-master
  labels:
    name: ubuntu-tol-master
spec:
  containers:
  - name: ubuntu
    image: docker.io/ubuntu:18.04
    ports:
    command: ["/usr/bin/tail"]
    args: ["-f", "/dev/null"]
  tolerations:
    - key: "node-role.kubernetes.io/master"
      operator: "Exists"
      effect: "NoSchedule"
#--------------------------------

deploy a pod with tolerations for node-role.kubernetes.io/master:NoSchedule (exists) and in the podspec nodeName: <name of a master>
    - this results in that the pod gets scheduled / deployed on the master
apiVersion: v1
kind: Pod
metadata:
  name: ubuntu-tol-master
  labels:
    name: ubuntu-tol-master
spec:
  containers:
  - name: ubuntu
    image: docker.io/ubuntu:18.04
    ports:
    command: ["/usr/bin/tail"]
    args: ["-f", "/dev/null"]
  nodeName: <name of a master node>
  tolerations:
    - key: "node-role.kubernetes.io/master"
      operator: "Exists"
      effect: "NoSchedule"
#--------------------------------

deploy a pod with nodeName in podspec
    - results in that the pod gets scheduled / deployed on the master, even though it does not tolerate the taints of the master
apiVersion: v1
kind: Pod
metadata:
  name: ubuntu-tol-master
  labels:
    name: ubuntu-tol-master
spec:
  containers:
  - name: ubuntu
    image: docker.io/ubuntu:18.04
    ports:
    command: ["/usr/bin/tail"]
    args: ["-f", "/dev/null"]
  nodeName: <name of a master node>
#--------------------------------

deploy a pod with nodeAffiny set to node-role.kubernetes.io/master exists 
    - Results in error, as the pod requires to be deployed on a node with that label, but the pod does not tolerate the taints of those nodes.
apiVersion: apps.openshift.io/v1
kind: DeploymentConfig
metadata:
  labels:
  name: ubuntu-run-node-affinity
spec:
  replicas: 1
  selector:
    name: ubuntu-run-node-affinity
  template:
    metadata:
      labels:
        name: ubuntu-run-node-affinity
    spec:
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: node-role.kubernetes.io/master
                operator: Exists
      containers:
      - command:
        - sleep
        - "6000"
        image: docker.io/ubuntu:18.04
        name: ubuntu-run-node-affinity