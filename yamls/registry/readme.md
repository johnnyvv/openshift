If there is no storageclass configured when OCP deployed, the registry operator puts it state to Removed

oc patch configs.imageregistry.operator.openshift.io/cluster --type=json  -p '[{"op":"replace","path":"/spec/storage","value":{"pvc":{"claim":<"PVC NAME">}}}]'
oc patch configs.imageregistry.operator.openshift.io/cluster --type=json  -p '[{"op":"replace","path":"/spec/storage","value":{"pvc":{"claim":"pvc-ocp-registry"}}}]'
oc patch configs.imageregistry.operator.openshift.io/cluster --type=json  -p '[{"op":"replace","path":"/spec/storage","value":{"emptyDir":{}}}]'