

or use oc run to create deployment config
```
oc run pgadmin4-run --image=docker.io/dpage/pgadmin4 --env="PGADMIN_DEFAULT_PASSWORD=welkom" --env="PGADMIN_DEFAULT_EMAIL=admin@admin.local" --env="PGADMIN_LISTEN_PORT=8080"
```

to do:
If the pod is created as a pod by a cluster admin, it will auto get the scc anyuid.
When the app is created through a deployment config, it will get the default restricted SCC.
but then it gives the following error:
```
PermissionError: [Errno 13] Permission denied: '/var/lib/pgadmin/sessions'
[2020-03-08 16:16:51 +0000] [19] [INFO] Worker exiting (pid: 19)
WARNING: Failed to set ACL on the directory containing the configuration database: [Errno 1] Operation not permitted: '/var/lib/pgadmin'
[2020-03-08 16:16:51 +0000] [1] [INFO] Shutting down: Master
[2020-03-08 16:16:51 +0000] [1] [INFO] Reason: Worker failed to boot.
```

To fix, run it as anyuid  or fix the image by chowning it to group 0 


