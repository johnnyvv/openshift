#!/bin/bash

set -e

printf "pljava.classpath = '/usr/lib/postgresql/9.6/lib/pljava.jar'\n\
        pljava.vmoptions = '-Xms32M -Xmx64M -XX:ParallelGCThreads=2'\n\
        work_mem = 5MB\n\
        pljava.libjvm_location = '/lib/libjvm.so'" >> /var/lib/postgresql/data/postgresql.conf