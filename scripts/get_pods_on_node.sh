#!/bin/env bash

#need to figure out how to improve speed.. well its obvious with all the -A
#takes a node name as arg and gives you all the pods that are running on that node
#still need to improve it using python or go


if oc get node $1 > /dev/null 2>&1 ; then
  pods=$(oc get pods -A -o jsonpath="{.items[?(@.spec.nodeName==\"$1\")].metadata.name}")
  echo "NAMESPACE --  PODNANE -- STATE"
  for i in $pods; do
    ns=$(oc get pods -A -o jsonpath="{.items[?(@.metadata.name==\"$i\")].metadata.namespace}")
    state=$(oc get pods -n $ns -o jsonpath="{.items[?(@.metadata.name==\"$i\")].status.phase}")
    if [[ "${state}" = "Running" ]]; then
      echo -e "${ns} -- ${i} -- \e[32m${state}\e[0m"
    else
      echo -e "${ns} -- ${i} -- \e[91m${state}\e[0m"
    fi

  done
else
  echo "Node ${1} not found"
  exit 1
fi