#!/bin/bash

if [[ ! $(id -u) -eq 0 ]] ; then
  echo "Script must run as root, exiting.."
  exit 1
fi

function usage {
    echo "$0 <size in GB> <share mode - default is ReadWRiteOnce>"
}

if [[ -z $1 ]] ; then
  echo "You need to specify PV size in GB, exiting.."
  usage
  exit 1
fi

nfs_server=$(nmcli -g ipv4.addresses connection show ens224 | awk -F '/' '{print $1}') # maybe easier to just set it static
nfs_root=/nfs

share_name=$(uuidgen)
share_mode="${2:-ReadWriteOnce}"

export_dir=/etc/exports.d
export_opts="(rw,no_root_squash,no_wdelay)"
export_acl="*"

if [[ -d "${nfs_root}/${share_name}" ]] ; then
  echo "Share ${nfs_root}/${share_name} already exists, exiting.."
  exit 1
fi

#creating dir
mkdir $nfs_root/$share_name

#add to exports
echo "${nfs_root}/${share_name} ${export_acl}${export_opts}" > $export_dir/$share_name.exports

#refresh exports
exportfs -r

echo "Created share ${nfs_root}/${share_name}"

echo "PV yaml:"
cat << EOF
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-$share_name
spec:
  capacity:
    storage: ${share_sizeGi} 
  accessModes:
    - $share_mode
  nfs:
    path: $nfs_root/$share_name
    server: $nfs_server
  persistentVolumeReclaimPolicy: Retain
EOF