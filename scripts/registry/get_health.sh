
for i in $(cat /tmp/images) ; do
  tag=$(echo $i | cut -d ":" -f 2)
  repo=$(echo $i | cut -d ":" -f 1)
  header="accept: application/json"
  curl -X GET "https://catalog.redhat.com/api/containers/v1/repositories/registry/registry.access.redhat.com/repository/$repo/tag/$tag" -s -H '$header'  | jq --arg image "$repo:$tag" '.data[] | select(.freshness_grades[-1].grade !="A") |  {id:._id,image:$image,grades:[.freshness_grades[].grade]}' 2> /dev/null
done
