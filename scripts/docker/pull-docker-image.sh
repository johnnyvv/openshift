#!/bin/bash

##
#docker login is required before running this
##

image=$1
src_repo=quay.io/XX/images #yes, thats my username will create a different / more appropriate one later 8|
dest_repo=<dest registry>/images
proxy_file=/root/scripts/set-proxy.sh

if [[ -z "${image}" ]] ; then echo "Please provider an image to pull" && exit 1 ;fi

if [[ -z "${https_proxy}" ]] ; then source $proxy_file ;fi

docker pull $src_repo:$image
docker tag $src_repo:$image $dest_repo:$image
#docker tag $src_repo:$image images:$image
docker push $dest_repo:$image
docker rmi $src_repo:$image $dest_repo:$image

echo "To pull the image: ${dest_repo}:${image}"

#call it like bash /root/scripts/pull-docker-image.sh <tag/image>
#call it like bash /root/scripts/pull-docker-image.sh httpd

#if you want to pull from another internal registry, you can use this one after the script above used:
##################################################################################################################

#!/bin/bash
 
##
#Docker login is required before running this script.
##
 
image=$1
src_repo=<source registry>/images #central registry
dest_repo=<dest registry>/images
 
if [[ -z "${image}" ]] ; then echo "Please provider an image to pull" && exit 1 ;fi
 
docker pull $src_repo:$image
docker tag $src_repo:$image $dest_repo:$image
#docker tag $src_repo:$image images:$image
docker push $dest_repo:$image
docker rmi $src_repo:$image $dest_repo:$image
 
echo "To pull the image: $dest_repo:$image"
