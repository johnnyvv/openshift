#!/bin/env python3

import urllib.request
import urllib.parse
import json
import csv

input_file="./images"
base_url="https://catalog.redhat.com/api/containers/v1/"

result = []

with open(input_file) as f:
    x = f.read().splitlines()

for image in x:

    result_image = {} 
    
    if len(image.split(':')) != 2:
        tag = "latest"
    else:
        tag = image.split(':')[1]
    repo=image.split(':')[0]
    y = urllib.request.urlopen(base_url + f"repositories/registry/registry.access.redhat.com/repository/{repo}/tag/{tag}") 
    y = json.loads(y.read().decode('utf-8'))
    
    result_image = {"image_id":y['data'][0]['_id'], "image":f"{repo}:{tag}","image_grades": [ i['grade'] for i in y['data'][0]['freshness_grades']], "latest_grade": y['data'][0]['freshness_grades'][-1]['grade']}

    y = urllib.request.urlopen(base_url + f"images/id/{result_image['image_id']}/vulnerabilities")
    y = json.loads(y.read().decode('utf-8'))
            
    result_image['severities'] = [ i['severity'] for i in y['data']]
    result_image['cves'] = [ i['cve_id'] for i in y['data']]
    result_image['advisories'] = [ i['severity'] + '--' + i['advisory_type'] + ':' + i['advisory_id'] for i in y['data']]
    
    result.append(result_image)


    #, "grades": ','.join(y['data'][0]['images_grades']), "latest_grade": [ i['grade'] for i in y['data'][0]['freshness_grades']],

print(len(result))

with open('openshift-app-images.csv', 'wt') as csvwrite:
    a = csv.writer(csvwrite)
    a.writerow(('image_id', 'image', 'grades', 'latest_grade', 'cves', 'advisories'))
    for row in result:
            a.writerow((row['image_id'],row['image'], ','.join(row['image_grades']), row['latest_grade'], ','.join(row['cves']), ','.join(row['advisories'])))
