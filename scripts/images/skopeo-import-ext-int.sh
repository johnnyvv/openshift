#!/bin/bash
#give a file with images (repo + tag) to mirror from the remote mirror (remote_reg) into the internal registry (int_reg)

int_reg=$1
input_file=$2
remote_reg=registry.redhat.io

proxy=1
proxy_file=/root/scripts/set-proxy.sh
auth_file=/root/pull-secret.json

if [[ "${proxy}" -eq 1 ]] ; then
  echo "Checking proxy settings..."
  if [[ -z "${https_proxy}" ]] ; then source $proxy_file ;fi
fi

echo "Mirroring $(wc -l < $input_file) image(s)."
for image in $(cat $input_file) ; do
#  tag=$(echo $image | cut -d "/" -f 2,3)
 echo $image
  skopeo copy --authfile=$auth_file --dest-tls-verify=false docker://$remote_reg/$image docker://$int_reg/$image
  if [[ $? -eq 0 ]] ; then echo $tag >> /tmp/success-bulk ; else echo $tag >> /tmp/failed-bulk ; fi
done