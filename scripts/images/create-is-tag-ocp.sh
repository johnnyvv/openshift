#!/bin/bash
#file with images that you want to import from private registry
#requiers internal registry name and input file which contains the repositories + tags
#<repo>:tag in ths script the first part of the repo is removed

int_reg=$1
input_file=$2

#for image in $(cat $input_file | cut -d "/" -f 2); do
for image in $(cat $input_file); do
  tag=$(echo $image | cut -d ":" -f 2)
  repo=$(echo $image | cut -d "/" -f 2 | cut -d ":" -f 1)
  echo "full: ${image}"
  echo "$repo ---- $tag"
  
  oc create imagestream $repo
  oc create imagestreamtag $repo:$tag --from-image=$int_reg/$image
done