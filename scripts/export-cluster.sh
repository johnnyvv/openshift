#!/bin/bash

case $1 in 

  <cluster 1>)
    export KUBECONFIG=<KUBE CONFIG PATH>
    export PS1="[\u@\h \e[0;0;35m<CLUSTER ID>\e[0m \W]\$ "
    ;;

  <cluster Y>)
    export KUBECONFIG=<KUBE CONFIG PATH>
    export PS1="[\u@\h \e[0;0;36m<CLUSTER ID>\e[0m \W]\$ "
    ;;

  <cluster X>)
    export KUBECONFIG=<KUBE CONFIG PATH>
    export PS1="[\u@\h \e[0;0;33m<CLUSTER ID>\e[0m \W]\$ "
    ;;

  *)
    ;;
esac
