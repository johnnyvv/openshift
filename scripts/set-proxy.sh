#!/bin/bash

#usage
#source <file>

proxy_server=<your proxy server> #this script assumes that there is 1 proxy for http and https traffic
export http_proxy="${proxy_server}"
export https_proxy="${proxy_server}"
export HTTP_PROXY="${proxy_server}"
export HTTPS_PROXY="${proxy_server}"
export no_proxy=10.0.0.0/8,172.17.0.0/16 #add local domains / ranges as you wish
export NO_PROXY=10.0.0.0/8,172.17.0.0/16
