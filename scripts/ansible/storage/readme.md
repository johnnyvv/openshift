Test for creating nfs share with ansible and create PV in Openshift using container + serviceaccount + curl

```
curl -k -X POST -d @./t.json -H "Accept: application/json" -H "Content-Type: application/json" \
-H "Authorization: Bearer $(cat /var/run/secrets/kubernetes.io/serviceaccount/token)" "https://$API_SERVER:6443/api/v1/persistentvolumes"
```
or 
```
curl -k -X POST -d '
{
        "apiVersion": "v1",
        "kind": "PersistentVolume",
        "metadata": {
                "name": "pv01-random-right"
        },
        "spec": {
                "accessModes": [
                        "ReadWriteOnce"
                ],
                "capacity": {
                        "storage": "1Mi"
                },
                "nfs": {
                        "path": "/nfs/openshift/myrandonnamebro",
                        "server": "$NFS_SERVER"
                },
                "persistentVolumeReclaimPolicy": "Retain"
        }
}' \
-H "Accept: application/json" -H "Content-Type: application/json" \
-H "Authorization: Bearer $(cat /var/run/secrets/kubernetes.io/serviceaccount/token)" "https://$API_SERVER:6443/api/v1/persistentvolumes"
```

As pod:
```
oc run test --image=ubi8 --restart=Never --serviceaccount=pv-sa --command -- bash -c 'echo "{\"apiVersion\": \"v1\",\"kind\": \"PersistentVolume\",\"metadata\": {\"name\": \"pv01-random-right\"},\"spec\": {\"accessModes\": [\"ReadWriteOnce\"],\"capacity\":{\"storage\": \"1Mi\"},\"nfs\":{\"path\": \"/nfs/openshift/myrandonnamebro\",\"server\": \"172.16.142.158\"},\"persistentVolumeReclaimPolicy\": \"Retain\"}}" > /tmp/pv.json && \
curl -k -X POST -d @/tmp/pv.json -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer $(cat /var/run/secrets/kubernetes.io/serviceaccount/token)" "https://api.os4.all.bdk:6443/api/v1/persistentvolumes"'
```
