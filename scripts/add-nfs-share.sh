#!/bin/bash

if [[ ! $(id -u) -eq 0 ]] ; then
  echo "Script must run as root, exiting.."
  exit 1
fi

nfs_root=/nfs
share_name=$(uuidgen)
export_dir=/etc/exports.d
export_opts="(rw,no_root_squash,no_wdelay)"
export_acl="*"

if [[ -d "${nfs_root}/${share_name}" ]] ; then
  echo "Share ${nfs_root}/${share_name} already exists, exiting.."
  exit 1
fi

#creating dir
mkdir $nfs_root/$share_name

#add to exports
echo "${nfs_root}/${share_name} ${export_acl}${export_opts}" > $export_dir/$share_name.exports

#refresh exports
exportfs -r

echo "Created share ${nfs_root}/${share_name}"
