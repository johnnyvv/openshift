#/bin/env bash
#simple script that executes a query ($1) in all of the dns pods.. currently no error handling ( when query returns nxdomain or .. need to add..
#./query-dns-ocp.sh google.com
dns_project="openshift-dns"
for i in $(oc get pods -n $dns_project | grep -v NAME | grep Running | awk '{print $1}') ; do
   node=$(oc get pods -n $dns_project -o jsonpath="{.items[?(@.metadata.name==\"$i\")].spec.nodeName}")
   printf "${i} -- $(oc exec -n $dns_project -c dns $i -- dig @localhost -p 5353 +short +search $1) -- $node\n"
done
