#!/bin/bash

release_chn=stable
release_ver=4.5
release_url=https://mirror.openshift.com/pub/openshift-v4/clients/ocp/$release_chn-$release_ver
#example http://mirror.openshift.com/pub/openshift-v4/clients/ocp/stable-4.5/
installer=openshift-install-linux.tar.gz
cli_client=openshift-client-linux.tar.gz
target_dir="./bin"
#download installer
curl -L "${release_url}/${installer}" | tar xz
#ls $installer || curl -O -L "${release_url}/${installer}"
#download client
curl -L "${release_url}/${cli_client}" | tar xz
