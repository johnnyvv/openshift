import socket,struct
import os,sys
#create a tcp socket
#print(os.environ)
rm_addr = None
if os.getenv('REMOTE_HOST'):
    rm_addr = os.getenv('REMOTE_HOST')
    print(f"Remote host set to: {rm_addr}") 
else:
    print("Remote host not set, exiting..")
    sys.exit(1)

resolv_file = "/etc/resolv.conf" if os.path.isfile("/etc/resolv.conf") else None
#print(resolv_file)
with open(resolv_file,'r') as f:
    resolv_data = f.readlines()

name_srvs = []
search = []
print(resolv_data)
for line in resolv_data:
    #remove leading spaces
    line = line.lstrip().strip()
    if line[0] == "#":
        print("Line is a comment")
    if "nameserver" in line[0:10]:
        name_srvs.append(line)
        print("Line is a nameserver")
    if "search" in line[0:6]:
        search.append(line.split(" ",1)[1])
        print(f"Search is {line.split(' ')[1]}")

print(search)