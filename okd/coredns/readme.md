
Run it
```
docker run -d --rm -p 5353:5353/udp -p 5353:5353/tcp -e LISTEN_PORT=5353 -e FORWARDER=8.8.8.8 -v "$(pwd)"/coredns:/coredns:ro coredns/coredns:latest -conf /conf/dnsconf
```
optionally build it
```
docker build -t <prefix>-coredns:<tag> .
```
run it after building
```
docker run -d --rm -p 5353:5353/udp -p 5353:5353/tcp -e FORWARDER=8.8.8.8 -e LISTEN_PORT=5353 jvv-coredns:0.2
```

Test config -> replace names with your own
```
dig @localhost -p 5353 +short -t srv _etcd-server-ssl._tcp.ocp.jvv.local.
dig @localhost -p 5353 +short -t srv _etcd-server-ssl._tcp.ocp.jvv.local. | awk '{print $4}' | cut -d "." -f1-4 | xargs dig +short @localhost -p 5353
```
