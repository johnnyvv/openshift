from flask import Flask, jsonify
from os import environ
from datetime import datetime
from faker import Faker

app = Flask(__name__)

LISTEN_PORT = environ.get("LISTEN_PORT", 8080)
VERSION = environ.get("APP_VERSION", "unknown")

fake = Faker() 

@app.route('/')
def index():
#    time = datetime.now().strftime("%H:%M:%S")
    if VERSION == "v0.1":
        resp = {
                'first_name' : fake.first_name(),
                'last_name' : fake.last_name(),
                'phone_number' : '',
                'version' : VERSION
                 }
        return jsonify(resp)
    resp = {
            'first_name' : fake.first_name(),
            'last_name' : fake.last_name(),
            'phone_number' : fake.phone_number(),
            'version' : VERSION
            }
    return jsonify(resp)

if __name__ == '__main__':
    app.run(host="0.0.0.0",port=LISTEN_PORT)          
