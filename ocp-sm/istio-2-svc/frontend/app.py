from flask import Flask, jsonify
from os import environ
from datetime import datetime
import requests,json

app = Flask(__name__)

LISTEN_PORT = environ.get("LISTEN_PORT", 8080)
VERSION = environ.get("APP_VERSION", "unknown")
BACKEND = environ.get("BACKEND", "personinfo")

@app.route('/')
def index():
#    time = datetime.now().strftime("%H:%M:%S")


    try:
        backend_resp = requests.get(f'http://{BACKEND}:8080/').json()
    except requests.exceptions.RequestException as e:
        print(e)
    #    return jsonify(resp) #need to check how to return if error
    resp = {
            'time' : datetime.now().strftime("%H:%M:%S"),
            'frontend_version' : VERSION,
            'backend_version' : backend_resp['version'],
            'first_name' : backend_resp['first_name'], 
            'last_name' : backend_resp['last_name'], 
            'phone_number' : backend_resp['phone_number'] 
            }
    return jsonify(resp)


if __name__ == '__main__':
    app.run(host="0.0.0.0",port=LISTEN_PORT)          
