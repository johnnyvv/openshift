### Example my by Istio team! ###
Im just leeching it for demo purposes!
Also examples taken from the Openshift docs example

# Steps #
Make sure your project is part of the istio-system (istio controlplane - ServiceMeshMemberRole)

Deploy the applications
```
oc apply -f https://raw.githubusercontent.com/Maistra/istio/maistra-1.1/samples/bookinfo/platform/kube/bookinfo.yaml
```
Deploy the virtual gateway + virtual service
```
https://raw.githubusercontent.com/Maistra/istio/maistra-1.1/samples/bookinfo/networking/bookinfo-gateway.yaml
```

To test application, get the route that is connected to the Istio ingress service
```
oc -n istio-system get route istio-ingressgateway -o jsonpath='{.spec.host}'
export GATEWAY_URL=$(oc -n istio-system get route istio-ingressgateway -o jsonpath='{.spec.host}')
```
Now go to the url with uri /productpage


Now to test the subsets, you can create destination rules that create subsets of app versions. 
Reference the subset in your virtual service and the traffic will only be sent to that version backend
```
oc apply -f https://raw.githubusercontent.com/Maistra/istio/maistra-1.1/samples/bookinfo/networking/destination-rule-all.yaml
```
