from flask import Flask, jsonify
from os import environ
from datetime import datetime


app = Flask(__name__)

LISTEN_PORT = environ.get("LISTEN_PORT", 8080)
VERSION = environ.get("APP_VERSION", "unknown")
@app.route('/')
def index():
#    time = datetime.now().strftime("%H:%M:%S")
    resp = {
            'time' : datetime.now().strftime("%H:%M:%S"),
            'version' : VERSION,
            'msg' : 'Hello darkness'
            }
    return jsonify(resp)


if __name__ == '__main__':
    app.run(host="0.0.0.0",port=LISTEN_PORT)          
