
Run as
```
oc new-app my-custom-is~<git repo> --strategy=source --name my-custom-thing
```

Some notes to self


##steps##
1. s2i create my-s2i:latest ./local-dir #s2i create s2i-do288-httpd s2i-do288-httpd
2. edit the Dockerfile if you wish, e.g. different base image
3. podman build -t <tag> -f ./Dockerfile
4. podman images
5. s2i build test/test-app/ localhost/base-s2i:latest --as-dockerfile /tmp/x
6. cd /tmp && podman build -t base-s2i-result -f ./x

if using Openshift it checks if in your source directory there is:
  .s2i/bin  in that directory you can put your run / assemble scripts
