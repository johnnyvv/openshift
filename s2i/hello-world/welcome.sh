#!/bin/bash

counter=1

while true; do
  printf "$(date) --- Hello world from custom run --- ${counter}\n"
  sleep $counter
  let counter=counter+1
  if [[ "${counter}" -eq 20 ]]; then
    counter=1
  fi
done