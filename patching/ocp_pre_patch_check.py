import urllib.request
import ssl
import json
import os
import sys
import argparse
#checks -> olm, macfhine operator OK / not processing, nodes ready, cluster operators ready, cluster is not updating 

def update_report(kind, msg):
    report[kind].append(msg)

def api_call(api_ep): 
    request_headers={'Content-type': 'application/json', 'Authorization': f"Bearer {token}"}
    req = urllib.request.Request(f"{api_link}{api_ep}", headers=request_headers)
    try:
        return urllib.request.urlopen(req, context=ssl_context)
    except Exception as e:
        print(e)
        sys.exit(2)

def get_nodes_status():
    return_bool = True
#    return_dict = {} 
    data = json.loads(api_call(node_ep).read().decode('utf-8'))
    for node in data['items']:
        for condition in node['status']['conditions']:
            if condition['type'] == 'Ready':
                if condition['status'] != "True":
                     update_report('nodes',f"node {node['metadata']['name']} not ready")
                     return_bool = False
                     break
 #               return_dict[node['metadata']['name']] = {'ready': condition['status']}
    return return_bool

def get_operators_status():
#    return_dict = {} 
    return_bool = True
    data = json.loads(api_call(cluster_operator_ep).read().decode('utf-8'))
    for operator in data['items']:
        #return_dict[operator['metadata']['name']] = []
        for condition in operator['status']['conditions']:
            if condition['type'] == 'Progressing':
                if condition['status'] != "False":
                    update_report('operators',f"operator {operator['metadata']['name']} not ready")
                    return_bool = False          
                    break
                     #return_dict[operator['metadata']['name']].append(True)
            if condition['type'] == 'Degraded':
                if condition['status'] != "False":
                    update_report('operators',f"operator {operator['metadata']['name']} not ready")
                    return_bool = False          
                    break
            if condition['type'] == 'Available':
                if condition['status'] != "True":
                    update_report('operators',f"operator {operator['metadata']['name']} not ready")
                    return_bool = False          
                    break
    return return_bool

def get_clusterversion_status():
#checks if cluster whether cluster is updating or not
    return_bool = True 
    data = json.loads(api_call(cv_ep).read().decode('utf-8'))
    for x in data['items']:
        for condition in x['status']['conditions']:
            if condition['type'] == 'Available':
                if condition['status'] != "True":
                    update_report('clusterversion',f"cluster might be updating or cvo degraded")
                    return_bool = False
                    break
            if condition['type'] == 'Failing':
                if condition['status'] != "False":
                    update_report('clusterversion',f"cluster might be updating or cvo degraded")
                    return_bool = False
                    break
            if condition['type'] == 'Progressing':
                if condition['status'] != "False":
                    update_report('clusterversion',f"cluster might be updating or cvo degraded")
                    return_bool = False
                    break
    return return_bool    

def get_mcp_status():
    return_bool = True
    data = json.loads(api_call(mcp_ep).read().decode('utf-8'))
    #print(json.dumps(data))
    for pool in data['items']:
        if pool['status']['machineCount'] != pool['status']['readyMachineCount']: 
            return_bool = False
        if pool['spec']['paused']: 
            return_bool = False
        for condition in pool['status']['conditions']:
            if condition['type'] == 'Degraded': 
                if condition['status'] != "False":
                    update_report('machineconfigpool',f"pool {pool['metadata']['name']} not ready")
                    return_bool = False
                    break
            if condition['type'] == 'NodeDegraded': 
                if condition['status'] != "False":
                    update_report('machineconfigpool',f"pool {pool['metadata']['name']} not ready")
                    return_bool = False
                    break
            if condition['type'] == 'Updating': 
                if condition['status'] != "False":
                    update_report('machineconfigpool',f"pool {pool['metadata']['name']} not ready")
                    return_bool = False
                    break
            if condition['type'] == 'Updated': 
                if condition['status'] != "True":
                    update_report('machineconfigpool',f"pool {pool['metadata']['name']} not ready")
                    return_bool = False
                    break
    return return_bool
 
def get_ocp_pod_status():
    return_bool = True
    namespace_list = []
    healthy_pod_status = ['Running', 'Completed', 'Succeeded']
    data = json.loads(api_call(namespace_ep).read().decode('utf-8'))
#    print(json.dumps(namespace_list))
   
    for namespace in data['items']:
        if namespace['metadata']['name'].startswith("openshift-"):
            namespace_list.append(namespace['metadata']['name'])

    for namespace in namespace_list: 
        data = json.loads(api_call(f"{pod_ep}/{namespace}/pods").read().decode('utf-8'))
        if namespace == 'openshift-logging':
            break
        for pod in data['items']:
            if pod['status']['phase'] not in healthy_pod_status:
                update_report('openshift_pods',f"pod {namespace}/{pod['metadata']['name']} not healthy")
                return_bool = False

    return return_bool
   

def check_health_status(health_dict):
    # here health status will be checked and output will say if cluster OK to patch or not
    healthy = []
    for item, state in health_dict.items():
        #check is state is True, if so append name of item to healhty list
        if state:
            healthy.append(item)
  
    if len(healthy) == len(health_dict):
        return True
   
    return False

def generate_health_report(input_dict, health_dict):
    report_dict = {'status': []} 
    for item, state in input_dict.items():
        if state:
            report_dict['status'].append({item:{'state':'Healthy', 'reason': ''}})
        if not state: 
            report_dict['status'].append({item:{'state':'Unhealthy', 'reason': health_dict[item]}})

    return report_dict
    

if __name__ == "__main__":

#parser configuration 

    parser = argparse.ArgumentParser()
    
    parser.add_argument('-a','--api',action="store", dest="api_link", help="The api endpoint to talk to OCP", type=str, required=False)
    parser.add_argument('-r','--report',action="store_true", dest="report", help="If you want to get a health report instead of exit code")
    parser.add_argument('--insecure',action="store_true", dest="insecure", help="If cert is not valid / trusted use this flag")

    args = parser.parse_args()

#var definitions
    api_link = None
    if os.getenv("OCP_API"):
        api_link = os.environ['OCP_API']
    if args.api_link and not api_link:
        api_link = args.api_link
    if not args.api_link and not api_link:
        print(f"Did not receive OCP API endpoint, please set env var OCP_API or use {sys.argv[0]} --api <api url>")
        sys.exit(2)

    if args.insecure:
        ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
        ssl_context.verify_mode = ssl.CERT_NONE
        ssl_context.check_hostname = False
   
    if not args.insecure:
        ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
        ssl_context.verify_mode = ssl.CERT_OPTIONAL
        ssl_context.check_hostname = False
    
#check token
    token = os.environ.get("OCP_TOKEN", "")
    if not token:
        print("token not valid, please set env var OCP_TOKEN with value of the token")
        sys.exit(2)
#endpoints
    node_ep = '/api/v1/nodes'
    cluster_operator_ep = '/apis/config.openshift.io/v1/clusteroperators'
    cv_ep = '/apis/config.openshift.io/v1/clusterversions'
    mcp_ep = '/apis/machineconfiguration.openshift.io/v1/machineconfigpools'
    namespace_ep = '/api/v1/namespaces'
    pod_ep = '/api/v1/namespaces'

#init health status
    report = {
        "operators": [],
        "nodes": [],
        "clusterversion" : [],
        "machineconfigpool" : [],
        "openshift_pods": []
    }

    health_status = {
        "operators": False,
        "nodes": False,
        "clusterversion": False,
        "machineconfigpool": False,
        "openshift_pods": False
    }

#get health status of checks
    if get_clusterversion_status():
        health_status['clusterversion'] = True

    if get_nodes_status():
        health_status['nodes'] = True

    if get_operators_status():
        health_status['operators'] = True

    if get_mcp_status():
        health_status['machineconfigpool'] = True

    if get_ocp_pod_status():
        health_status['openshift_pods'] = True

#generate output 
if args.report:
        print(json.dumps(generate_health_report(health_status,report)))
        sys.exit(0)
        

    if check_health_status(health_status):
        sys.exit(0)
    else:
        sys.exit(1)
