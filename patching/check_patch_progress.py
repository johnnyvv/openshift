import urllib.request
import ssl
import json
import os
import sys
import argparse
import datetime
#checks -> olm, macfhine operator OK / not processing, nodes ready, cluster operators ready, cluster is not updating


class ENDPOINTS():
    node = '/api/v1/nodes'
    cluster_operator = '/apis/config.openshift.io/v1/clusteroperators'
    cvo = '/apis/config.openshift.io/v1/clusterversions'
    mcp = '/apis/machineconfiguration.openshift.io/v1/machineconfigpools'
    namespace = '/api/v1/namespaces'
    pod = '/api/v1/namespaces'
    
def api_call(api_ep):
    request_headers={'Content-type': 'application/json', 'Authorization': f"Bearer {token}"}
    req = urllib.request.Request(f"{api_link}{api_ep}", headers=request_headers)
    try:
        return urllib.request.urlopen(req, context=ssl_context)
    except Exception as e:
        print(e)
        sys.exit(2)


def check_cvo_status(target_version):
# checks if cluster is updating and what the status is - print message also if available
# we will return false right away if some things are not right

    #set this to true if you want to skip version validation
    version_ok = True

    #check if cvo  desired version matches the target-version arg supplied to this script. Kind of a double script
    desired_version_match = False

    data = json.loads(api_call(ENDPOINTS.cvo).read().decode('utf-8'))
    
    #check if desired version equals taget-version
    if data['items'][0]['status']['desired']['version'] == target_version:
        desired_version_match = True
        summary['version'] = "ok"
    else:
        summary['version'] = f"target-version {target_version} does not match cluster desired version {data['items'][0]['status']['desired']['version']}"

    for item in data['items']:
    #check version
        for history in item['status']['history']:
#            if datetime.datetime.strptime(history['startedTime'],'%Y-%m-%dT%H:%M:%SZ').date() == date_test.date():
            if datetime.datetime.strptime(history['startedTime'],'%Y-%m-%dT%H:%M:%SZ').date() == datetime.datetime.now().date():
                if history['version'] == target_version:
                    version_ok = True
                    break
                else:
                    summary['cvo']['version'] = "cluster version is not yet equal to target-version"
                    break
            else:
                summary['cvo']['version'] = "no entry yet in cvo history that matches todays date"

#check conditions
        for condition in item['status']['conditions']:
            if condition['type'] == 'Available':
                if condition['status'] != 'True':
                    summary['cvo']['available'] = False
                    if 'message' in condition:
                        summary['cvo']['message'] = condition['message']
                    return False
            if condition['type'] == 'Failing':
                if condition['status'] != 'False':
                    summary['cvo']['failing'] = True
                    if 'message' in condition:
                        summary['cvo']['message'] = condition['message']
                    return False
            if condition['type'] == 'Progressing':
                if condition['status'] != 'False':
                    summary['cvo']['progressing'] = True
                    if 'message' in condition:
                        summary['cvo']['message'] = condition['message']
                    return False

    if version_ok and desired_version_match:
        return True

    return False

def check_operators_version(target_version):

#here we check if the openshift operators are on the desired version
#still not sure about the output thats why not returing False right away..

    return_list = []

    data = json.loads(api_call(ENDPOINTS.cluster_operator).read().decode('utf-8'))

#get version of operator and check if operator is healthy / ready
    for operator in data['items']:
        for condition in operator['status']['conditions']:
            if condition['type'] == 'Progressing':
                if condition['status'] != 'False':
                     return_list.append({'name': operator['metadata']['name'], 'version' : operator['status']['versions'][0]['version'], 'ready': False})
                     summary['operators'].append({'name': operator['metadata']['name'], 'version' : operator['status']['versions'][0]['version'], 'ready': False})
                     break
            if condition['type'] == 'Degraded':
                if condition['status'] != 'False':
                     summary['operators'].append({'name': operator['metadata']['name'], 'version' : operator['status']['versions'][0]['version'], 'ready': False})
                     return_list.append({'name': operator['metadata']['name'], 'version' : operator['status']['versions'][0]['version'], 'ready': False})
                     break
            if condition['type'] == 'Available':
                if condition['status'] == 'True':
                     summary['operators'].append({'name': operator['metadata']['name'], 'version' : operator['status']['versions'][0]['version'], 'ready': True})
                     return_list.append({'name': operator['metadata']['name'], 'version' : operator['status']['versions'][0]['version'], 'ready': True})
                     break
                     
#check if operator version is equal to target_version 
#    for index, operator in enumerate(return_list):
    for index, operator in enumerate(summary['operators']):
        if operator['version'] == target_version:
            summary['operators'][index]['updated'] = True
            if not operator['ready']:
                return False 
        else:
            summary['operators'][index]['updated'] = False

    return True


def check_mcp_status(role):
# check if nodes of MCP X are updated / ready
    nodes_ready = False
   
    data = json.loads(api_call(f"{ENDPOINTS.mcp}/{role}").read().decode('utf-8'))
  
    if data['status']['machineCount'] == data['status']['readyMachineCount']:
        nodes_ready = True
   
    for condition in data['status']['conditions']:
        if condition['type'] == 'Degraded':
            if condition['status'] != 'False':
                return False
        if condition['type'] == 'NodeDegraded':
            if condition['status'] != 'False':
                return False
        if condition['type'] == 'Updating':
            if condition['status'] != 'False':
                return False
        if condition['type'] == 'Updated':
            if condition['status'] == 'True' and nodes_ready:
                return True
            else:
                return False
   

def check_node_status():
# check that nodes are reporting healthy / ready.. need to see if this is any different than checking the output of MCP. For now skipping this check
    pass
  

#main
if __name__ == "__main__":

#parser configuration

    parser = argparse.ArgumentParser()

    parser.add_argument('-tv','--target-version',action="store", dest="target_version", help="The version that cluster is updating to", type=str, required=False)#required True later
    parser.add_argument('-a','--api',action="store", dest="api_link", help="The api endpoint to talk to OCP", type=str, required=False)
    parser.add_argument('--insecure',action="store_true", dest="insecure", help="If you OK with insecure HTTPS connection (not trusted certificates)")

    args = parser.parse_args()

#var definitions
    api_link = None
    if os.getenv("OCP_API"):
        api_link = os.environ['OCP_API']
    if args.api_link:
        api_link = args.api_link
    if not args.api_link and not api_link:
        print(f"Did not receive OCP API endpoint, please set env var OCP_API or use {sys.argv[0]} --api <api url>")
        sys.exit(2)
        #O   api_link = os.environ.get("OCP_API", args.api_link)

    if args.insecure:
        ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
        ssl_context.verify_mode = ssl.CERT_NONE
        ssl_context.check_hostname = False

    if not args.insecure:
        ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
        ssl_context.verify_mode = ssl.CERT_OPTIONAL
        ssl_context.check_hostname = False

#check token
    token = os.environ.get("OCP_TOKEN", "")
    if not token:
        print("token not valid, please set env var OCP_TOKEN with value of the token")
        sys.exit(2)

    #summary to be printed why cluster is not yet done patching
    summary = {
        "cvo": {},
        "version": "",
        "operators": []
    }

    health_status = {
        "operators": False,
        "master_pool": False,
        "worker_pool": False,
        "updated": False,
        "desired_version" : False
    }
    #check_operators_version(args.target_version) 


    if check_cvo_status(args.target_version):
        health_status['updated'] = True
        health_status['desired_version'] = True

    if check_operators_version(args.target_version):
        health_status['operators'] = True

    if check_mcp_status('worker'):
        health_status['worker_pool'] = True

    if check_mcp_status('master'):
        health_status['master_pool'] = True

    #do the actual output
    patch_done = True
    ok = u'\u2713'
    nok = u'\u2716'
    tab = '\t'

    for key, value in health_status.items():
        if not value:
            print(f"{key} ready {2 * tab} [{nok}]")
            patch_done = False
        if value:
            print(f"{key} ready {2 * tab} [{ok}]")

    if patch_done:
        sys.exit(0)
    else:
        print(json.dumps(summary, indent=4, sort_keys=True))
        sys.exit(1)
