# Updating OCP4.X in disconnected env #

Please make sure that you know to which version you want to upgrade, and that the upgrade is available for your current version.
Check the following:
* https://ctron.github.io/openshift-update-graph
* The release.txt of a given OCP release. It should tell you if you can upgrade to the release from your current version


## Steps ##
To update, follow these steps

* Download the oc binary using script "ocp-dl-artifacts.sh <target version>" (Edit script to change the target directory)
* Run script "generate-signature-cm.sh <target version>" This will create a configmap (checksum-<target version>.yaml) which you need to apply to your cluster(oc apply -f ) #this makes sure you dont need to use --force when updating
* Download the release images and push them to your internal registry, for this run script "pull-images.sh <target version> <internal registry hostname>" This will return ImageContentSourcePolicy this you need to apply to your server (it might already be there so please check if you need to apply it or not! But make sure, because without this the upgrade will not work!!
* Generate the update command to use, for this use script "generate-update-cmd.sh <target version> <internal registry hostname>" This also checks with skopeo that the image is available with the digest OCP uses to upgrade. If ready and skopeo find the image simply copy the last string in the output (oc adm upgrade...) and paste it somewhere to start the update




Need to fix the markup someday
