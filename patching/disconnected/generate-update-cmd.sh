#!/bin/bash

if [[ -z "${1}" ]] ; then echo "Please provide ocp version, e.g. 4.5.24 -- Exiting..." ; exit 1 ; fi

source /root/scripts/set-proxy.sh


base_dir=/data/openshift-artifacts
OCP_RELEASE_NUMBER=$1
LOCAL_REGISTRY=$2
LOCAL_REPOSITORY=ocp/openshift4
ARCHITECTURE=x86_64
DIGEST="$(${base_dir}/${OCP_RELEASE_NUMBER}/oc adm release info quay.io/openshift-release-dev/ocp-release:${OCP_RELEASE_NUMBER}-${ARCHITECTURE} | sed -n 's/Pull From: .*@//p')"
DIGEST_ALGO="${DIGEST%%:*}"
DIGEST_ENCODED="${DIGEST#*:}"

#echo $DIGEST

#check if image exists
skopeo inspect --authfile=/root/pull-secret.json docker://${LOCAL_REGISTRY}/${LOCAL_REPOSITORY}@${DIGEST}


if [[ "${?}" -eq 0 ]] ; then
  echo "oc adm upgrade --allow-explicit-upgrade --to-image ${LOCAL_REGISTRY}/${LOCAL_REPOSITORY}@${DIGEST}"
else
  echo "Skopeo did not find the image -> ${LOCAL_REGISTRY}/${LOCAL_REPOSITORY}@${DIGEST}"
fi