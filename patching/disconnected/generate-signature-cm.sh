#!/bin/bash

if [[ -z "${1}" ]] ; then echo "Please provide ocp version, e.g. 4.5.24 -- Exiting..." ; exit 1 ; fi

source /root/scripts/set-proxy.sh


base_dir=/data/openshift-artifacts
OCP_RELEASE_NUMBER=$1
ARCHITECTURE=x86_64
DIGEST="$(${base_dir}/${OCP_RELEASE_NUMBER}/oc adm release info quay.io/openshift-release-dev/ocp-release:${OCP_RELEASE_NUMBER}-${ARCHITECTURE} | sed -n 's/Pull From: .*@//p')"
DIGEST_ALGO="${DIGEST%%:*}"
DIGEST_ENCODED="${DIGEST#*:}"
SIGNATURE_BASE64=$(curl -s "https://mirror.openshift.com/pub/openshift-v4/signatures/openshift/release/${DIGEST_ALGO}=${DIGEST_ENCODED}/signature-1" | base64 -w0 && echo)

cat >checksum-${OCP_RELEASE_NUMBER}.yaml <<EOF
apiVersion: v1
kind: ConfigMap
metadata:
  name: release-image-${OCP_RELEASE_NUMBER}
  namespace: openshift-config-managed
  labels:
    release.openshift.io/verification-signatures: ""
binaryData:
  ${DIGEST_ALGO}-${DIGEST_ENCODED}: ${SIGNATURE_BASE64}
EOF