#!/bin/bash

if [[ -z "${1}" ]] ; then echo "Please provide ocp version, e.g. 4.5.24 -- Exiting..." ; exit 1 ; fi

if [[ -z $https_proxy ]] ; then
  source /root/scripts/set-proxy.sh
fi
version=$1
base_dir=/data/openshift-artifacts
release_url=https://mirror.openshift.com/pub/openshift-v4/clients/ocp/$version

installer=openshift-install-linux.tar.gz
cli_client=openshift-client-linux.tar.gz
#download installer
#currently not needed as we need to create our own installer
#curl -L "${release_url}/${installer}" | tar xz
#ls $installer || curl -O -L "${release_url}/${installer}"
#download client

mkdir $base_dir/$version
cd $base_dir/$version
curl -L "${release_url}/${cli_client}" | tar xz
