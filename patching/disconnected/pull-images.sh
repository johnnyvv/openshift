#!/bin/bash

if [[ -z "${1}" || -z "${2}" ]] ; then echo "run script like ${0} <ocp version> <internal registry>  -- Exiting..." ; exit 1 ; fi

base_dir=/data/openshift-artifacts

OCP_RELEASE=$1
LOCAL_SECRET_JSON=/root/pull-secret.json  #make sure creds for local registry are added!
#LOCAL_SECRET_JSON=$HOME/okd/install/pull-secret.json  #make sure creds for local registry are added!
LOCAL_REGISTRY=$2
LOCAL_REPOSITORY='ocp/openshift4'
PRODUCT_REPO='openshift-release-dev'
RELEASE_NAME='ocp-release'
ARCHITECTURE='x86_64'
#oc_bin=$(if [[ ! -z "${oc_bin}" ]]; then echo $oc_bin; else echo $(which oc); fi)

#oc_bin="/data/openshift-artifacts/${OCP_RELEASE}/oc"
#chmod u+x $oc_bin

if [[ -z $https_proxy ]] ; then
  source /root/scripts/set-proxy.sh
fi

echo "Using proxy ${https_proxy}"

$base_dir/$OCP_RELEASE/oc adm -a ${LOCAL_SECRET_JSON} release mirror  --insecure \
--from=quay.io/${PRODUCT_REPO}/${RELEASE_NAME}:${OCP_RELEASE}-${ARCHITECTURE} \
--to=${LOCAL_REGISTRY}/${LOCAL_REPOSITORY} \
--to-release-image=${LOCAL_REGISTRY}/${LOCAL_REPOSITORY}:${OCP_RELEASE}-${ARCHITECTURE}