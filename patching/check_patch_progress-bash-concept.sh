#!/bin/bash

target_version=$1
#target_version=
ll_updated=false

co_updated=false
nodes_updated=false
workers_updated=false
masters_updated=false

date_today=$(date +"%Y-%m-%d")
cyan='\033[0;36m'
purple='\033[1;35m'
yellow='\e[93m'
green='\e[92m'

nc='\033[0m'

check_co () {
  IFS=$'\n'
  cluster_operators=$(oc get co --no-headers | wc -l)
  cluster_operators_ready=0
  cluster_operators_patched=0
  for co in $(oc get co --no-headers) ; do
    status=""
    name=$(echo $co | awk '{print $1}')
    version=$(echo $co | awk '{print $2}')
    ready=$(echo $co | awk '{print $3}')
    progressing=$(echo $co | awk '{print $4}')
    degraded=$(echo $co | awk '{print $5}')

    if [[ "${ready}" == "True" ]] ; then status=ready ; fi
    if [[ "${progressing}" == "True" ]] ; then status=progressing ; fi
    if [[ "${degraded}" == "True" ]] ; then status=degraded ; fi

    #check if operators are updated.. if version == new version then will be checked if the co is also ready. Increment 2 counters
    if [[ "${version}" != "${target_version}" ]] ; then
      if [[ "${status}" == "ready" ]] ; then
        #printf "${cyan}Version${nc}: Cluster operator ${name} not yet on desired version, status is ${green}${status}${nc}\n"
        printf "${cyan}Version${nc}: Cluster operator ${name} not yet on ${yellow}desired version${nc}, status is ${green}${status}${nc}\n"
      fi
      if [[ "${status}" != "ready" ]] ; then
        printf "${cyan}Version${nc}: Cluster operator ${name} not yet on ${yellow}desired version${nc}, status is ${yellow}${status}${nc}\n"
      fi
    else
      cluster_operators_patched=$(( cluster_operators_patched + 1 ))
      if [[ "${ready}" == "True" && "${progressing}" == "False" && "${degraded}" == "False" ]] ; then cluster_operators_ready=$(( cluster_operators_ready + 1 )) ; fi
    fi

    if [[ "${status}" != "ready" ]] ; then
      printf "${purple}Status${nc}: Cluster operator ${name} in state ${yellow}${status}${nc}\n"
    fi

    if [[ "${ready}" == "True" && "${progressing}" == "False" && "${degraded}" == "False" ]] ; then cluster_operators_ready=$(( cluster_operators_ready + 1 )) ; fi

  done
  unset IFS

  check_nodes master
  check_nodes worker

  printf "${cluster_operators_patched}/${cluster_operators} ready\n"
  if [[ "${cluster_operators_ready}" -eq "${cluster_operators}" && "${cluster_operators_patched}" -eq "${cluster_operators}" ]] ; then
    printf "All operators are patched and ready\n"
    co_updated=true
  fi
}


check_nodes () {
  role=$1
  nodes_ready=$(oc get machineconfigpool $role -o json | jq .status.readyMachineCount)
  nodes_count=$(oc get machineconfigpool $role -o json | jq .status.machineCount)
  nodes_updated=$(oc get machineconfigpool $role -o json | jq .status.updatedMachineCount)
  nodes_unavail=$(oc get machineconfigpool $role -o json | jq .status.unavailableMachineCount)
  nodes_last_patch=$(oc get machineconfigpool $role -o json | jq -r .status.conditions[-1].lastTransitionTime | cut -d T -f 1)

  if [[ "${nodes_last_patch}" == "${date_today}" ]] ; then
    if [[ "${nodes_updated}" -eq "${nodes_count}" ]] ; then
      printf "${role} nodes  are succesfully updated\n"
      return 0
    else
      printf "Not all ${role} nodes are updated. So far updated ${nodes_updated} node(s) from the ${nodes_count} node(s)\n"
    fi
  echo ok
  fi
}

while oc get clusterversion --no-headers | egrep -i '%|not' ; do

 if oc get clusterversion | grep waiting > /dev/null ; then
   line=$(oc get clusterversion --no-headers)
   waiting_on=(${line// waiting / })
   echo ${waiting_on[1]}
 fi
  check_co

  while [[ "${co_updated}" = true && "${nodes_updated}"  != true ]] ; do
    if check_nodes master; then masters_updated=true ; fi
    if check_nodes worker; then workers_updated=true ; fi

    if [[ "${masters_updated}" = true && "${workers_updated}" = true ]] ; then
      nodes_updated=true
      printf "cannot"
    else
      sleep 10
    fi
  done

  if [[ "${nodes_updated}" = true && "${co_updated}" = true ]] ; then
    echo "patching done."
    break
  else
    printf "\n\n" && sleep 10
  fi
done