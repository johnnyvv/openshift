import json
import urllib.request
import sys
import argparse

#functions
def get_version(x, data):
    return data['nodes'][x]['version']

#"main"
def main():

    parser = argparse.ArgumentParser()

    parser.add_argument('-c','--channel', action="store", dest="channel", help="Channel to use for update path", type=str, required=True)
    parser.add_argument('-cv','--current-version', action="store", dest="current_version", help="current version of OCP cluster", type=str, required=True)
    parser.add_argument('-a','--architecture', action="store", dest="architecture", default="amd64", help="Architecture to use for search", type=str, required=False)
    parser.add_argument('--get-latest', action="store_true", help="if to only output latest or nothing")
    parser.add_argument('-cp', '--check-path', action="store_true", help="if you want to check if a certain version is still in upgrade path")
    parser.add_argument('-tv','--target-version', action="store", dest="target_version", help="current version of OCP cluster", type=str)
    parser.add_argument('--json', dest="json_out",  action="store_true", help="if you want to have output in json")

    args= parser.parse_args()

    #only these architectures are supported
    valid_architectures= ["amd64"]

    #setting some vars from argument values
    current_version=args.current_version
    channel=args.channel
    arch=args.architecture

#validations
    if arch not in valid_architectures:
        print(f"architecture {arch} not a valid architure")
        sys.exit(1)

    if len(current_version.split('.')) != 3 or not current_version.split('.')[2]:
        print(f"version is not valid. require X.Y.Z")
        sys.exit(1)

    #values for making api call
    api_link='https://api.openshift.com'
    api_endpoint=f"{api_link}/api/upgrades_info/v1/graph?channel={channel}&arch={arch}"
    request_headers={'Content-type': 'application/json'}

    valid_patches=[]
    index=0



    #get the version list
    req = urllib.request.Request(api_endpoint, headers=request_headers)
    try:
        get_content=urllib.request.urlopen(req)
    except Exception as e:
        print(e)

    data=json.loads(get_content.read().decode('utf-8'))

#get index count 
    for release in data['nodes']:
        if release['version'] == current_version:
            break
        index+=1

#find valid patches for current_version
    for path in data['edges']:
        if path[0] == index:
            valid_patches.append(get_version(path[1], data))

#sort the patches
    valid_patches.sort(key=lambda s: list(map(int, s.split('.'))))

#what to print to screen
    if valid_patches and not args.get_latest and not args.check_path:
        if args.json_out:
            print(json.dumps({"latest_version": valid_patches[-1], "paths_available" : valid_patches, "current_version" : current_version, "channel": channel}, indent=4, sort_keys=True))
        else:
            print(f"latest: {valid_patches[-1]}")
            print(f"other options: {valid_patches}")
            print(f"upgrade paths available: {len(valid_patches)}")
        sys.exit(0)

    if args.get_latest and valid_patches and not args.check_path:
        if args.json_out:
            print(json.dumps({"latest_version" : valid_patches[-1]}))
        else:
            print(valid_patches[-1])
        sys.exit(0)

    if args.check_path:
        if args.target_version in valid_patches:
            if args.json_out:
                print(json.dumps({"result": "ok"}))
            if not args.json_out:
                print("ok")
            sys.exit(0)
        print(f"version {args.target_version}  not in upgrade path anymore.")
        sys.exit(1)

    if not valid_patches:
        if args.json_out:
            print(json.dumps({"latest_version": "", "paths_available" : [], "current_version" : current_version, "channel": channel, "error": "no patches"}, indent=4, sort_keys=True))
            sys.exit(1)
        print(f"No patch available for version {current_version}")
        sys.exit(1)


if __name__ == "__main__":
    main()