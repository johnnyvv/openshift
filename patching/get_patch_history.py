import urllib.request
import ssl
import json
import os
import sys
import argparse
#checks -> olm, macfhine operator OK / not processing, nodes ready, cluster operators ready, cluster is not updating


class ENDPOINTS():
    cvo = '/apis/config.openshift.io/v1/clusterversions'
    

def update_report(kind, msg):
    report[kind].append(msg)

def api_call(api_ep):
    request_headers={'Content-type': 'application/json', 'Authorization': f"Bearer {token}"}
    req = urllib.request.Request(f"{api_link}{api_ep}", headers=request_headers)
    try:
        return urllib.request.urlopen(req, context=ssl_context)
    except Exception as e:
        print(e)
        sys.exit(2)


def get_patch_history():
# checks if cluster is updating and what the status is - print message also if available

    data = json.loads(api_call(ENDPOINTS.cvo).read().decode('utf-8'))
    
    for item in data['items']:
        for update in item['status']['history']:
            if update['state'] == 'Completed':
                print(f"Cluster updated succesfully to version {update['version']} on {update['startedTime']}")
            else:
                print(f"Cluster updated unsuccesfully to version {update['version']} on {update['startedTime']}")


if __name__ == "__main__":

#parser configuration

    parser = argparse.ArgumentParser()

    parser.add_argument('-a','--api',action="store", dest="api_link", help="The api endpoint to talk to OCP", type=str, required=False)
    parser.add_argument('--insecure',action="store_true", dest="insecure", help="If you OK with insecure HTTPS connection (not trusted certificates)")

    args = parser.parse_args()

#var definitions
    api_link = None
    if os.getenv("OCP_API"):
        api_link = os.environ['OCP_API']
    if args.api_link:
        api_link = args.api_link
    if not args.api_link and not api_link:
        print(f"Did not receive OCP API endpoint, please set env var OCP_API or use {sys.argv[0]} --api <api url>")
        sys.exit(2)
        #O   api_link = os.environ.get("OCP_API", args.api_link)

    if args.insecure:
        ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
        ssl_context.verify_mode = ssl.CERT_NONE
        ssl_context.check_hostname = False

    if not args.insecure:
        ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
        ssl_context.verify_mode = ssl.CERT_OPTIONAL
        ssl_context.check_hostname = False

#check token
    token = os.environ.get("OCP_TOKEN", "")
    if not token:
        print("token not valid, please set env var OCP_TOKEN with value of the token")
        sys.exit(2)

    get_patch_history()
