

## Dedicated storage nodes ##
Label the nodes
```
oc label node <node> [node..] cluster.ocs.openshift.io/openshift-storage=
```

#create infra node
```
oc label node <node> [node..] node-role.kubernetes.io/infra=
```

#taint the node so only approved pods can run on these nodes
```
oc adm taint nodes <node> [node..] node.ocs.openshift.io/storage="true":NoSchedule
```
 
#create OCS project
```
oc adm new-project openshift-storage
```