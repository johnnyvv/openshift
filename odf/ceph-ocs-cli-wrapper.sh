#!/bin/bash
label='app=rook-ceph-operator' 
ceph_conf='/var/lib/rook/openshift-storage/openshift-storage.config'
pod_name=$(oc get pods -n openshift-storage -l app=rook-ceph-operator -ojsonpath='{.items[0].metadata.name}')

if [[ -z $1 ]] ; then
  while true; do
    printf "$(date) -- $(oc exec -n openshift-storage $pod_name -- ceph -c $ceph_conf health)\n"
    sleep 2
  done
else
  oc exec -n openshift-storage $pod_name -- ceph -c $ceph_conf $@
fi
