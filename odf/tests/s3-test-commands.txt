cat << EOF >> .aws/credentials
[default]
aws_access_key_id = $AWS_ACCESS_KEY_ID
aws_secret_access_key = $AWS_SECRET_ACCESS_KEY
EOF

 

#for RGW port 80

aws s3 ls s3:// --endpoint http://$BUCKET_HOST
cat /etc/hostname > rgw-test-file

aws s3 cp rgw-test-file s3://$BUCKET_NAME  --endpoint=http://$BUCKET_HOST
#list bucket
aws s3 ls s3://$BUCKET_NAME --endpoint=http://$BUCKET_HOST
aws s3 ls s3://$BUCKET_NAME/rgw-test-file --endpoint=http://$BUCKET_HOST
#print remote file to stdout
aws s3 cp s3://$BUCKET_NAME/rgw-test-file - --endpoint=http://$BUCKET_HOST
 

#NooBaa

#set in your deployment / container env: [{'name':'AWS_CA_BUNDLE','value':'/run/secrets/kubernetes.io/serviceaccount/service-ca.crt'}]
aws s3 ls s3:// --endpoint https://$BUCKET_HOST --no-verify-ssl
cat /etc/hostname > noobaa-test-file
 

aws s3 cp noobaa-test-file s3://$BUCKET_NAME  --endpoint=https://$BUCKET_HOST [--no-verify-ssl]
aws s3 ls s3://$BUCKET_NAME/ --endpoint=https://$BUCKET_HOST [--no-verify-ssl]
aws s3 cp s3://$BUCKET_NAME/noobaa-test-file - --endpoint=http://$BUCKET_HOST [--no-verify-ssl]