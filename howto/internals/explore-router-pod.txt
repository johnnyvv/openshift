oc login -u system:admin -n default
oc get pods 
#get first router pod
oc get pods -l 'router=router' -o=jsonpath='{.items[0].metadata.name}'
#remote shell into it
oc rsh $(oc get pods -l 'router=router' -o=jsonpath='{.items[0].metadata.name}')
#config
/var/lib/haproxy/conf/haproxy.config
#dir /var/lib/haproxy/conf

#look inside
grep / sed or just vi