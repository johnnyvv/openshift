#Install docker
yum install docker -y
systemctl start docker && systemctl enable docker

cat >>  /tmp/docker << EOF
DEVS=/dev/xvdb
VG=docker-vg
STORAGE_DRIVER=overlay2
EOF

systemctl stop docker
rm -rf /var/lib/docker/*
docker-storage-setup
