oc adm manage-node --list-pods node1.example.com
oc adm manage-node --list-pods --selector <label>
oc get pods --all-namespaces -o wide


oc adm manage-node <node> --schedulable=false #to mark a node for pods, simple change --schedulable to =true
oc adm drain <node>

#might want to get all the values of the node in case you miss something
oc get node node -o yaml > /tmp/node.delete
#delete node from cluster
oc delete node <node>