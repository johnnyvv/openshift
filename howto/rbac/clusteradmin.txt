#Create a group
oc adm groups new cluster-administrators
#assign permissions to the group
oc adm policy add-cluster-role-to-group cluster-admin cluster-administrators
#add user to group
oc adm groups add-users cluster-administrators <username>
#oc get groups to verify





#OR YOU WANT TO ASSIGN TO A USER !! FINE...
oadm policy add-cluster-role-to-user cluster-admin <user_name>