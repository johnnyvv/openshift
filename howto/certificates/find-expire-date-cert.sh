#!/bin/bash

tmp=$(mktemp -d)
trap "cd - &>/dev/null; rm -rf $tmp" EXIT
cd $tmp

echo "Checking expiration dates for all certs in all namespaces ..." >&2
echo "(Pipe to 'sort' to see soonest-to-expire at the top)" >&2
echo >&2

for ns in $(oc get ns --no-headers | awk '{print $1}'); do
    for secret in $(oc get secrets -n $ns | awk 'BEGIN{IGNORECASE=1}; $2~/tls/ {print $1}'); do
        oc -n $ns extract secret/$secret --confirm --keys tls.crt >/dev/null || continue
        [[ -f tls.crt ]] || continue
        enddate=$(openssl x509 -noout -enddate -in tls.crt | cut -d= -f2)
        echo -e "$(date --date="$enddate" +"%F %R %Z")\t$ns / $secret"
    done
done