#!/bin/bash
#curl -u docker:docker https://registry2.jvv.local:5000/v2/ocp4/openshift44-11/tags/list
#echo username:password | base64

OCP_RELEASE='4.5.3'
LOCAL_SECRET_JSON=/tmp/pull-secret.json  #make sure creds for local registry are added!
#LOCAL_SECRET_JSON=$HOME/okd/install/pull-secret.json  #make sure creds for local registry are added!
LOCAL_REGISTRY='registry2.jvv.local:5000'
LOCAL_REPOSITORY='ocp4/openshift'
PRODUCT_REPO='openshift-release-dev'
RELEASE_NAME='ocp-release'
ARCHITECTURE='x86_64'
oc_bin=$(if [[ ! -z "${oc_bin}" ]]; then echo $oc_bin; else echo $(which oc); fi)

echo "Downloading images for version ${OCP_RELEASE} with oc version $($oc_bin version --client)"

$oc_bin adm -a ${LOCAL_SECRET_JSON} release mirror  --insecure \
--from=quay.io/${PRODUCT_REPO}/${RELEASE_NAME}:${OCP_RELEASE}-${ARCHITECTURE} \
--to=${LOCAL_REGISTRY}/${LOCAL_REPOSITORY} \
--to-release-image=${LOCAL_REGISTRY}/${LOCAL_REPOSITORY}:${OCP_RELEASE}-${ARCHITECTURE}
