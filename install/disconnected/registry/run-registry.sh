#!/bin/bash

##
# need to add better check to see if selinux is enabled.
# to trust a certificate do the following
# for docker, create a directory mkdir /etc/docker/certs.d/<cn>:<port>/domain.crt e.g.
# cp <cert> /etc/docker/certs.d/registry.my.domain:5000
# for podman
#  cp <cert> /etc/pki/ca-trust/source/anchors/ && update-ca-trust extract
#
# Add support for volumes
# create them first -> use them in run -> will be default
##

reg_port=5000
reg_image="docker.io/registry:2"
reg_vol=registry_vol
base_dir=$HOME/docker-registry

if docker ps > /dev/null 2>&1 ; then
  container_runtime=docker
  selinux_opts=
else podman ps > /dev/null 2>&1  #one one machine docker / podman installed.. which is preferred need to check
  container_runtime=podman
  selinux_opts=":Z"
fi

if [[ -z $container_runtime ]]; then
  echo "unknown container runtime, exiting..."
  exit 1
fi
#create htpasswd file
mkdir -p $base_dir/{auth,certs} 

if [[ ! -f "${base_dir/auth/htpasswd}" ]]; then
  htpasswd -Bbn docker docker > $base_dir/auth/htpasswd
fi
#generated certificates
#openssl req -newkey rsa:4096 -nodes -sha256 -keyout $base_dir/certs/domain.key -x509 -days 365 -out $base_dir/certs/domain.crt

if [[ ! -f "${base_dir}/certs/domain.key" ]] ; then
  echo "No certs found in directory: ${base_dir}/certs"
  echo "Please generate the key by looking in file for instructions ${PWD}/${0}" 
  echo "openssl req -newkey rsa:4096 -nodes -sha256 -keyout $base_dir/certs/domain.key -x509 -days 365 -out $base_dir/certs/domain.crt"
  exit 1
fi

#create volume no check now if its already exists.. 
$container_runtime volume create $reg_vol

#registry_mount=$($container_runtime volume inspect $rev_vol  | jq '.[0].Mountpoint')

$container_runtime run -d \
-p $reg_port:$reg_port --restart=always --name registry \
-v $base_dir/auth:/auth$selinux_opts \
-e REGISTRY_HTTP_ADDR=0.0.0.0:$reg_port \
-e REGISTRY_AUTH=htpasswd \
-e "REGISTRY_AUTH_HTPASSWD_REALM=Registry Realm" \
-e REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd \
-v $base_dir/certs:/certs$selinux_opts \
-v $reg_vol:/var/lib/registry$selinux_opts \
-e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
-e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
$reg_image
