#!/bin/bash

#use following if installing okd so you can use another oc binary. or just set it if you want to use a spefic version
#oc_bin=/home/jj/okd/bin/oc

if [[ -z "${oc_bin}" ]] ; then  oc_bin=$(which oc) ; fi

echo "Using oc binary ${oc_bin} with version $($oc_bin version --client)" 

OCP_RELEASE='4.5.3'
LOCAL_SECRET_JSON=$HOME/okd/install/pull-secret.json #make sure creds for local registry are added!
LOCAL_REGISTRY='registry2.jvv.local:5000'
LOCAL_REPOSITORY='ocp4/openshift'
PRODUCT_REPO='openshift-release-dev'
RELEASE_NAME='ocp-release'
ARCHITECTURE='x86_64'

$oc_bin adm -a ${LOCAL_SECRET_JSON} release extract --insecure --command=openshift-install "${LOCAL_REGISTRY}/${LOCAL_REPOSITORY}:${OCP_RELEASE}-${ARCHITECTURE}"
