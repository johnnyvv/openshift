Edit your config file - haproxy.cfg

rebuild it:
```
docker build -t my-haproxy:0.X .
```
test it
```
docker run -it --rm --name haproxy-syntax-check my-haproxy:0.X haproxy -c -f /usr/local/etc/haproxy/haproxy.cfg
```

Finally run it
```
docker run -d --rm -p 8080:8080 -p 1936:1936 -p 8081:8081 my-haproxy:0.X
```
