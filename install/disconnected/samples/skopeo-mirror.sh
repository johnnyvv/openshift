#!/bin/bash
int_reg=$1
remote_reg=registry.redhat.io
proxy=1
proxy_file=/root/scripts/set-proxy.sh
input_file=/tmp/mirror
auth_file=/root/pull-secret.json

if [[ "${proxy}" -eq 1 ]] ; then
  echo "Checking proxy settings..."
  if [[ -z "${https_proxy}" ]] ; then source $proxy_file ;fi
fi

echo "Mirroring $(wc -l < $input_file) image(s)."

for image in $(cat $input_file) ; do
 tag=$(echo $image | cut -d "/" -f 2,3)
  echo $tag
  skopeo copy --authfile=$auth_file --dest-tls-verify=false docker://$image docker://$int_reg/$tag
  if [[ $? -eq 0 ]] ; then echo $tag >> /tmp/success ; else echo $tag >> /tmp/failed ; fi
done