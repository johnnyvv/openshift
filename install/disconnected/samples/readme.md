#edit samples registry
```
oc edit configs.samples cluster```
samplesRegistry: <name of internal / alternative registry>
```

#get list of images to mirror
```
oc get is -n openshift -o yaml | grep 'name: <name of internal / alternative registry>'  | awk '{print $2}'
```

```
#!/bin/bash
int_reg=<name of internal / alternative registry>
remote_reg=registry.redhat.io
source /root/scripts/set-proxy.sh
#read file with list of images to mirror

for image in $(cat /tmp/mirror); do
  tag=$(echo $image | cut -d "/" -f 2,3)
  echo "Mirroring image ${tag}"
  skopeo copy --authfile=/root/pull-secret.json --dest-tls-verify=false docker://$image docker://$int_reg/$tag
done
```