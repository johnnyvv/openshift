#!/bin/bash
#only use for images that failed to copy using skopeo
#requires docker or podman, in case of podman change the binary in this script
int_reg=$1
remote_reg=registry.redhat.io
proxy=1
proxy_file=/root/scripts/set-proxy.sh
input_file=/tmp/failed
auth_file=/root/pull-secret.json
if [[ "${proxy}" -eq 1 ]] ; then
  echo "Checking proxy settings..."
  if [[ -z "${https_proxy}" ]] ; then source $proxy_file ;fi
fi

echo "Mirroring $(wc -l < $input_file) image(s)."

for tag in $(cat $input_file) ; do
#  tag=$(echo $image | cut -d "/" -f 2,3)
  echo $tag
  docker pull $remote_reg/$tag
  if [[ $? -eq 0 ]] ; then echo $tag >> /tmp/success-2 ; else echo $tag && break >> /tmp/failed-2 ; fi
  docker tag $remote_reg/$tag $int_reg/$tag
  docker push $int_reg/$tag
  docker rmi $int_reg/$tag $remote_reg/$tag
  if [[ $? -eq 0 ]] ; then echo $tag >> /tmp/success-2 ; else echo $tag >> /tmp/failed-2 ; fi
done