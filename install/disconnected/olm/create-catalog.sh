#!/bin/env bash
#REG_CREDS=/root/.docker/config.json
source /root/scripts/set-proxy.sh
#source ./quay-token.sh

#need to set umask - if set to 0077 container cannot run ->  therefor when running pod with this images cannot open bundles.db
umask 022

#ocp_bin=$1
ocp_version=$1
int_reg=$2
int_reg_tag=v1
int_creds=/root/pull-secret.json

function usage {
 echo "$0 <ocp version> <internal_registry>"
 echo -e "Example:\n$0 4.5.14 registry.example.com"
}

if [[ -z "${ocp_version}" || -z "${int_reg}" ]] ; then echo "Missing parameter(s)" && usage && exit 1 ;fi
#if [[ -z "${ocp_version}" ]] || [[ -z "${int_reg} ]] ; then echo "Missing parameter(s)" && usage && exit 1 ;fi
#if [[ -z "${oc_bin}" ]] ; then  oc_bin=$(which oc) ; fi

echo "umask is $(umask)"
#$oc_bin adm catalog build \ # assumes that oc is available in general directory and does not need to be overwritten
/data/openshift-artifacts/$ocp_version/oc adm catalog build \
-a $int_creds \
--appregistry-org redhat-operators \
--from=registry.redhat.io/openshift4/ose-operator-registry:v4.5 \
--to=$int_reg/operators/redhat-operators:$int_reg_tag \
--insecure --filter-by-os='linux/amd64'

#--loglevel 6 #> /tmp/build 2>&1 # use if need to debug more