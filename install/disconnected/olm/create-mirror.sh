#REG_CREDS=/root/.docker/config.json
source /root/scripts/set-proxy.sh
#source ./quay-token.sh

ocp_version=$1
int_reg=$2
int_reg_tag=v1
int_creds=/root/pull-secret.json

function usage {
 echo "$0 <ocp version> <internal_registry>"
 echo -e "Example:\n$0 4.5.14 registry.example.com"
}

if [[ -z "${ocp_version}" || -z "${int_reg}" ]] ; then echo "Missing parameter(s)" && usage && exit 1 ;fi

/data/openshift-artifacts/$ocp_version/oc adm catalog mirror \
-a $int_creds \
$int_reg/operators/redhat-operators:$int_reg_tag \
$int_reg \
--insecure --manifests-only
#--skip-verification true --insecure -a $REG_CREDS --loglevel 5