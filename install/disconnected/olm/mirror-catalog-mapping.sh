#REG_CREDS=/root/.docker/config.json
source /root/scripts/set-proxy.sh
#source ./quay-token.sh

ocp_version=$1

##
# some images might give error "not found"  like the ose-promtail - if so please remove these from your new-mapping.txt -- not sure how to check maybe with not like ose-promtail
#Remove promtail images from list - check later if these images are needed / working -> maybe check if necessary to write clause to check pull status
sed -i "/^registry.redhat.io\/openshift4\/ose-promtail/d" redhat-operators-manifests/new-mapping-duplicates.txt

#check if there are duplicates before mirroring - if duplicates mirroring will give an error
sort redhat-operators-manifests/new-mapping-duplicates.txt | uniq >  redhat-operators-manifests/new-mapping.txt

#this actually pulls the images in new-mapping.txt and pushes them to internal registry which is defined in the new-mapping.txt file
# Often this does not push it correctly as the deployment on openshift failds due to invalid / missmatch @ image digest -> check copy-skopeo.sh 
/data/openshift-artifacts/$ocp_version/oc image mirror \
-f ./redhat-operators-manifests/new-mapping.txt \
--insecure -a /root/pull-secret.json