#need to file a way to auto get latest db file or assume only one db- directory in /tmp

db_dir=$1

#check if database file directory exists
if [[ ! -f "/tmp/${db_dir}/bundles.db" ]] ; then echo "no valid database file, exiting..." && exit 1 ; fi

#find operators / images related to logging / elasticsearch 
echo "select * from related_image where operatorbundle_name like '%logg%' OR operatorbundle_name like '%elastic%';" | sqlite3 -line /tmp/$db_dir/bundles.db | grep image | sed -e 's/^[[:space:]]*//' > redhat-operators-manifests/images
sed -i s/'image = '/''/g redhat-operators-manifests/images

#find the images you need - not yet remove any duplicates
for i in $(cat redhat-operators-manifests/images); do grep "${i}" redhat-operators-manifests/mapping.txt ; done > redhat-operators-manifests/new-mapping-duplicates.txt