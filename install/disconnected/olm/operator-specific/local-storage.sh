db_dir=$1
if [[ ! -f "/tmp/${db_dir}/bundles.db" ]] ; then echo "no valid database file, exiting..." && exit 1 ; fi

echo "select * from related_image where operatorbundle_name like '%local-storage%';" | sqlite3 -line /tmp/$db_dir/bundles.db | grep image | sed -e 's/^[[:space:]]*//' > redhat-operators-manifests/images
sed -i s/'image = '/''/g redhat-operators-manifests/images

for i in $(cat redhat-operators-manifests/images); do grep "${i}" redhat-operators-manifests/mapping.txt ; done > redhat-operators-manifests/new-mapping-duplicates.txt
