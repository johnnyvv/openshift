db_dir=$1

if [[ ! -f "/tmp/${db_dir}/bundles.db" ]] ; then echo "no valid database file, exiting..." && exit 1 ; fi
echo "select * from related_image where operatorbundle_name like '%pipeline%' ;" | sqlite3 -line /tmp/$db_dir/bundles.db | grep image | sed -e 's/^[[:space:]]*//' > redhat-operators-manifests/images-pipeline

sed -i s/'image = '/''/g redhat-operators-manifests/images-pipeline
#remove images from docker.io and google cloud #can remove the lines underneath if you have access to the container registries. 
sed -i '/docker.io/d' redhat-operators-manifests/images-pipeline
sed -i '/cloud-sdk/d' redhat-operators-manifests/images-pipeline

for i in $(cat redhat-operators-manifests/images-pipeline); do grep "${i}" redhat-operators-manifests/mapping.txt ; done > redhat-operators-manifests/new-mapping-duplicates.txt