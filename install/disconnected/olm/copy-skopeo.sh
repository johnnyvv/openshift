#!/bin/bash
source /root/scripts/set-proxy.sh #if you need a proxy to go to the internet

int_reg=$1

function usage {
 echo "$0 <internal_registry>"
 echo -e "Example:\n$0 registry.example.com"
}


if [[ -z "${int_reg}" ]] ; then echo "Missing parameter(s)" && usage && exit 1 ;fi

#to auto generate to-copy.txt
#cut -d "@" -s -f 1  redhat-operators-manifests/new-mapping.txt   | sed s/'registry.redhat.io\/'// | uniq > to-copy.txt

#to copy manually generated 
# cut -d "@" -s -f 1  redhat-operators-manifests/new-mapping.txt   | sed s/'registry.redhat.io\/'// | uniq > to-copy.txt
#this can download / push a lot of images!!!!!! Maybe better to do it in digest.. well need to check
for i in $(cat /root/scripts/operators/to-copy.txt) ; do
  skopeo copy --authfile=/root/pull-secret.json --all docker://registry.redhat.io/$i docker://$int_reg/$i
done