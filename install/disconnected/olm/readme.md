# This is for disconnected OLM installation #
Version OCP used is 4.5.6

---



## Steps: ##
* ONE TIME or LCm -> First create image with the catalog for OCP, this is only needed once and you can update this one from time to time.
* RUN mirror-catalog.sh <ocp version> <internal registry>
* Create a script that gets the images for a specific operator -> like cluster-logging.sh or local-storage.sh
* Run mirror-catalog-mapping.sh <ocp version>
* If some fail, run it with copy-skopeo.sh <internal registry>


Sometimes, the mirror of the oc commando is not enough. Then you need to run copy-skopeo.sh

To copy individual images:
```
skopeo copy  --all --authfile=/root/pull-secret.json --dest-tls-verify=false docker://registry.redhat.io/<repo>@sha256:<digest> docker://<internal registry>/<repo>
```

To inspect (making sure image is actually there)
```
skopeo inspect --authfile=/root/pull-secret.json docker://<internal registry>/repo@sha256:<digest>
skopeo inspect --authfile=/root/pull-secret.json docker://<internal registry>/repo:tag
```

--- OLD
```
* Build the operator registry image for your version -> create-catalog.sh
* Get the manifests -> mirror-catalog.sh
* Pick what you want to be sycned / copied to your OCP cluster - If you don't cherrypick it, it will download lots of images!!!
** See extra-cms.sh for suggestions how to find images beloning to a certain operator / version
* Create a new mapping file to be used for mirroring! See extra-cms.sh for command # will try to make a script for it once I really get the hang of it
* Mirror the images -> mirror-catalog-mapping.sh
* If using a cherrypicked mappings.txt (new-mapping.txt) you can run generate-source.sh to generate ImageContentSourcePolicy with only the images in the new-mapping.txt file
Check the CSV of the operator to get the image + digest it will use. Experience tell that hte manifests / digest of the mirrored container images is not copied. 
If it is not copied, you can use the copy-skopeo.sh script to mirror the images once again, this time with the proper digests!
```
# ISSUES #
Need to figure out how it all works! Digests ruin it and seems to be a lot of work to get operators working properly in a disconnected environment
Also, when pulling cluster logging for 4.5.0 it says it is made for OKD...
