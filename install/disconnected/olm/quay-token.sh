source /root/scripts/set-proxy.sh

AUTH_TOKEN=$(curl -sH "Content-Type: application/json" \
    -XPOST https://quay.io/cnr/api/v1/users/login -d '
    {
        "user": {
            "username": "'"<username>"'",
            "password": "'"<password>"'"
        }
    }') 
AUTH_T=$(echo $AUTH_TOKEN | awk -F ":" '{print $2}' | cut -d "}" -f 1 )

export AUTH_T
