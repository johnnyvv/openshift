int_reg=$1
operator=$2

if [[ -z "${int_reg}" || -z "${operator}" ]] ; then echo "Please provide internal registry and operator name" && exit 1 ; fi

#need to make sure that we create a clean yaml file. Therefor we need to delete duplicate entries
cut -d "@" -s -f 1 redhat-operators-manifests/new-mapping.txt  | sort | uniq > redhat-operators-manifests/source-entries.txt


echo "apiVersion: operator.openshift.io/v1alpha1
kind: ImageContentSourcePolicy
metadata:
  name: redhat-operators-$operator
spec:
  repositoryDigestMirrors:" > new-source-$operator.yaml

for i in $(cat redhat-operators-manifests/source-entries.txt)
do
  y=$(echo $i | awk -F '@' '{print $1}' | cut -d "/" -s -f 2,3)
#do x=$(echo $i | awk -F '@' '{print $1}' | cut -d "/" -s -f 1) && y=$(echo $i | awk -F '@' '{print $1}' | cut -d "/" -s -f 2,3)
cat << EOF >> new-source-$operator.yaml
  - mirrors:
    - $int_reg/$y
    source: registry.redhat.io/$y
EOF
done
