from os import path, environ

#set the base dir to dir this file is located in
basedir = path.abspath(path.dirname(__name__))

class Config():

    def get_env_var(key):
        return environ.get(key, "404")        


    DEBUG = True
    TMP_DIR = '/tmp'
    #DATABASE_FILE = path.join(basedir,get_env_var("DATABASE_FILE"))
    DATABASE_FILE = path.join(TMP_DIR,environ.get("DATABASE_FILE","default.db"))
    CONFIG_FILE = path.join(basedir,(__file__))
    APP_NAME = "OCP Demo"
    BASE = basedir

    DATABASE_NAME = get_env_var('MYSQL_DATABASE')
    DATABASE_USER = get_env_var('MYSQL_USER')
    DATABASE_PASSWORD =  get_env_var('MYSQL_PASSWORD')
    DATABASE_HOST = get_env_var('MYSQL_HOST')
    DATABASE_CSTRING = 'mysql+pymysql://'+DATABASE_USER+':'+DATABASE_PASSWORD+'@'+DATABASE_HOST+'/'+DATABASE_NAME

    #SQLALCHEMY_DATABASE_URI="sqlite:///"+DATABASE_FILE
    SQLALCHEMY_DATABASE_URI=DATABASE_CSTRING
    SQLALCHEMY_ECHO = environ.get('SQLALCHEMY_ECHO', False) #https://docs.sqlalchemy.org/en/13/core/engines.html#mysql
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    DEPLOYMENT_NAME = '404'
    env_vars = environ.items()
    for var in env_vars:
            if "_SERVICE_HOST" in var[0]:
                        DEPLOYMENT_NAME = var[0].split('_SERVICE')[0]
