## 2 ways to deploy the stress-app ##
```
oc new-app --name php-stress https://bitbucket.org/johnnyvv/openshift --context-dir=dockerfiles/php-stress-cpu  --strategy=Docker
```
OR
```
oc apply -f ./php-stress-deploy.yaml
```

## Set limits if not set before ##
```
oc set resources dc/php-stress --limits=cpu=1000m
```

oc describe PodMetrics  <pod>
oc describe PodMetrics 
oc adm top pod   <pod>

oc autoscale dc/php-stress --min 1 --max 2 --cpu-percent=60 #scale when cpu goes over 60%
oc autoscale deployment/php-stress-min 1 --max 2 --cpu-percent=60 #scale when cpu goes over 60%
oc get hpa php-stress -w  # watch it!