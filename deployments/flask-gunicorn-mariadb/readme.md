Deploy mariadb -- set the values to variables to the values in ./env-file
```
oc ..
```

Deploy it like
```
oc new-app https://bitbucket.org/johnnyvv/openshift.git --context-dir=deployments/flask-gunicorn-mariadb --env-file=./env-file --name flask-db-app
```

or just build it

```
oc new-build https://bitbucket.org/johnnyvv/openshift.git --context-dir=deployments/flask-gunicorn-mariadb --env-file=./env-file --name flask-db-build-app
```

use it 
```
<route>/create?name=<some string>&nickname=<another string>
<route>/users
```