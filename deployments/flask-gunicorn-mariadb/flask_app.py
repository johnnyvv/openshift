from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import exc
from os import environ

app = Flask(__name__)
app.config
app.config.from_object('flask_config.Config')

db = SQLAlchemy(app)

class User(db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True, nullable=False)
    nickname = db.Column(db.String(30), unique=True, nullable=False)

    def __repr__(self):
        return f"<User {self.nickname}>"

class Company(db.Model):
    __tablename__ = "companies"

    id = db.Column(db.Integer, primary_key=True)
    company_name = db.Column(db.String(40), nullable=False)
    contact_person = db.Column(db.String(50), nullable=False)

db.create_all()

@app.route('/')
def index():
    return "Hello there"

@app.route('/info')
def info():
    return jsonify(project=environ.get('OPENSHIFT_BUILD_NAMESPACE','404'),
                build=environ.get('OPENSHIFT_BUILD_NAME','404'),
                deployment_name=app.config['DEPLOYMENT_NAME'],
                service_ip=environ.get(app.config['DEPLOYMENT_NAME']+'_SERVICE_HOST',404),
                basedir=app.config['BASE'],)

@app.route('/create')
def create_user():
    name = request.args.get('name')
    nickname = request.args.get('nickname')
    
    if isinstance(name,str) and isinstance(nickname,str):
        #create the user
        db.session.add(User(name=name, nickname=nickname))
        try:
            db.session.commit()
            return jsonify(message=f"added user {name}")
        except exc.IntegrityError as err:
            db.session.rollback()
            return jsonify(message=f"user {name} already exists") 
    else:
        return jsonify(message="failed to create user")

@app.route('/users')
def get_users():
    users = User.query.all()
    ret_arr = []
    for user in users:
        ret_arr.append({'name': user.name, 'nickname': user.nickname })
    return jsonify(users=ret_arr)
    
       

