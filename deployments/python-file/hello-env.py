import time
from os import environ

#check if env var GREETING_NAME is set, else say hello to stranger bcecause its the polit(i)e thing to do.
#deploy like oc new-app python~https://bitbucket.org/johnnyvv/openshift --context-dir=dockerfiles/python-file/ --name py-s2i -e APP_FILE=hello-env.py
while True:
    print(f"Hello {environ.get('GREETING_NAME', 'stranger')}")
    time.sleep(5)