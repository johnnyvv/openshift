from flask import Flask
from flask import request
from flask import jsonify

from socket import gethostname
from os import environ
app = Flask(__name__)

deployment_name = '404'
env_vars = environ.items()
for var in env_vars:
    if "_SERVICE_HOST" in var[0]:
        deployment_name = var[0].split('_SERVICE')[0] 
@app.route('/')
def index():
    return f"Hello from Flask!\nThis app is running on a pod with name {gethostname()}."

@app.route('/info')
def info():
    return jsonify(project=environ.get('OPENSHIFT_BUILD_NAMESPACE','404'),
                build=environ.get('OPENSHIFT_BUILD_NAME','404'),
                deployment_name=deployment_name,
                service_ip=environ.get(deployment_name+'_SERVICE_HOST',404),
                pod_hostname=gethostname(),)

@app.route('/env')
def get_env_vars():
    env_vars = environ.items()
    b = {}
    for var in env_vars:
        b[var[0]] = var[1]
    return jsonify(b)

#app.run(host='0.0.0.0',port=8080, debug=True)
