deploying like
```
oc new-app https://bitbucket.org/johnnyvv/openshift --context-dir=dockerfiles/flask-gunicorn -e APP_MODULE="wsgi:app" -e APP_CONFIG=gunicorn.conf.py --name guni-flask-jvv
```

to delete all, for testing purposes. Might need to replace app value.. Risk is yours
```
oc delete all -l app=guni-flask-jvv 
```