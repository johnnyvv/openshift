package main

import (
        "fmt"
        "net/http"
        "os"
)

func main() {
        listenPort, exists := os.LookupEnv("LISTEN_PORT")
        if (exists) {
            http.HandleFunc("/", HelloServer)
            http.ListenAndServe(":"+listenPort, nil)
        }
        http.HandleFunc("/", HelloServer)
        http.ListenAndServe(":8080", nil)
}

func HelloServer (w http.ResponseWriter, r *http.Request) {
        hostname, err := os.Hostname()
        if err != nil {
                panic(err)
        }
        customMsg, exists := os.LookupEnv("CUSTOM_MSG")
        if (exists) {
            fmt.Fprint(w,"Welcome on host ", hostname, "\n")
            fmt.Fprint(w,customMsg)
        } else {
        fmt.Fprint(w,"Welcome on host ", hostname)
        }
}