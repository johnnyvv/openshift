package main

import (
	"fmt"
	"os"
	"time"
)

func main() {
	var containerHostname, err = os.Hostname()
	if err != nil {
		fmt.Printf("Failed to get hostname err: %s", err)
		os.Exit(1)
	}
	for true {
		fmt.Println("Hello from container", containerHostname)
		time.Sleep(2 * time.Second)
	}
}
