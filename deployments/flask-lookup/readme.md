deploying like
```
oc new-app https://bitbucket.org/johnnyvv/openshift.git --context-dir=deployments/flask-lookup -e APP_MODULE="wsgi:app" -e APP_CONFIG=gunicorn.conf.py --name query-jvv
```

to delete all, for testing purposes. Might need to replace app value.. Risk is yours
```
oc delete all -l app=query-jvv
```

Use it:

Get env of pod
```
<route>/env
```
Get dns query / info
```
<route>/lookup?query=<domain>
<route>/lookup?query=redhat.com
```

