import os
def get_resolv_info(section):
    resolv_file = "/etc/resolv.conf" if os.path.isfile("/etc/resolv.conf") else None
    with open(resolv_file,'r') as f:
        resolv_data = f.readlines()
    name_srvs = []
    search = []

    for line in resolv_data:
    #remove leading spaces
        line = line.lstrip().strip()
        if line[0] == "#":
            continue         
            #print("Line is a comment")
        if "nameserver" in line[0:10]:
            name_srvs.append(line.strip("nameserver "))
            #print("Line is a nameserver")
        if "search" in line[0:6]:
            for s in line.split(" ",1)[1].strip().rsplit(" "):
                if s:
                    search.append(s)
    if section == "nameserver":
        return name_srvs
    if section == "search":
        return search
#    return [name_srvs,search]

print(get_resolv_info('nameserver'))

