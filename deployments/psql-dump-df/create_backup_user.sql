\c <db>;
-- variables are db , backup_role , backup_user , schemas -- 
CREATE ROLE <backup_role>;

GRANT CONNECT ON DATABASE $db TO pega_backup_role;

-- do the following for all your schemas
GRANT USAGE ON SCHEMA <schema> TO pega_backup_role;

-- do the following for all your schemas  
-- GRANT select on all tables include views and foreign tables
GRANT select ON ALL tables in schema <schema> to pega_backup_role;

-- do the following for all your schemas
GRANT select ON ALL sequences in schema <schema> to <backup_role>;

-- set default grants - do the following for all your schemas
ALTER DEFAULT PRIVILEGES IN SCHEMA <schema> GRANT select ON  TABLES to <backup_role>;

ALTER DEFAULT PRIVILEGES IN SCHEMA <schema> GRANT select ON  sequences to <backup_role>;
-- assign role to user
CREATE USER <backup_user> WITH PASSWORD '<password>';
GRANT <backup_role> TO <backup_user>;