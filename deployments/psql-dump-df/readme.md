## Sample backup container ##

## To do ##
* Add support for mount secrets
* Add deployment for job that runs X interval that also supports persistent storage to write backup to
* Create a way to restore a backup - thinking about read only pvc -> get roles of postgres if target is different -> pg_restore
         
Deploy on openshift:
```
 oc run test-backup-script --image=quay.io/bonkstok/psql-client-jvv:v0.1 --restart='Never' --env="db=<db name>" --env="db_host=<db_host>" --env="out_dir=/tmp" --env="backup_user=<user>" --env="PGPASSWORD=<pw>"
```

if you want to build it using openshift -> 
```
oc new-build --name psql-backup-test --binary=true
```
Now make sure you have the artifacts (dockerfile and backup script in the '--from-dir')
```
oc start-build psql-backup-test --from-dir=.
```

## Variables of the backup-script ##
The script that is used for the backup is backup-script.sh
it needs the following variables

### vars ###
* db -> name of the database you wish to backup
* db_host -> hostname / ip addr of the postgres cluster/server
* out_dir -> which directory the dump will be placed within the container. If this is backed up by persistent storage, you will need to mount it at this directory
* backup_user -> name of the backup user
* PGPASSWORD -> password of the backup user - Need to check if this can be done by creating from file..


## Creating a backup user for your database ##
If you choose to have a backup user for your database, you first need to create it.
Check the file create_backup_user.sql and edit it, after execute it in your postgres cluster
```
psql -f create_backup_user.sql #add auth if needed
```

## Create the cronjob ##
In the file psql-cronjob.yaml there is a cronjob object that you can use to create the cronjob.
Edit it as you find it, if you wish to have it write to persistent volume create the pv and pvc. 