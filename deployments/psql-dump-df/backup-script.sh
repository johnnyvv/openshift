#!/usr/bin/env bash
: `
#to do -> add support for secrets through mounts - head / cat the file for value.
db= #name of the database
db_host=  #ip or name of database host
out_dir= #where to place history / dump file
backup_user= 
PGPASSWORD=
`

umask 002

backup_max=4
backup_count=0
pgdump_binary="/usr/bin/pg_dump"
out_file="${out_dir}/${db}-$(date +'%d-%m-%Y').dump"
history_file="${out_dir}/${db}-backup.history"

if [[ -f "${history_file}" ]]; then
    latest_backup_file=$(tail -n 1 $history_file)
    backup_count=$(wc -l $history_file  | awk '{print $1}')
    echo "There are ${backup_count} backups -> max backups set to ${backup_max}"
    echo "Latest backup is ${latest_backup_file}"
    if [[ -f "${latest_backup_file}" ]]; then
        echo "Latest backup file still exists"
        latest_exists=1
    else
        echo "Latest backup file does not exist"
        latest_exists=0
    fi
else
    touch $history_file
fi

echo "Starting backup, writing to file ${out_file}"
$pgdump_binary -h $db_host -U $backup_user -w -Fc -Z 9 --file $out_file -d $db
#touch $out_file

if [[ "${?}" -ne 0 ]]; then
    echo "Backup failed, please do not use file ${out_file} - investigate and start new backup"
    succes=0
else
    echo "Backup succesful"
    echo $out_file >> $history_file
    succes=1
    backup_count=$(($backup_count + 1))
#    echo "b is ${backup_count}"
    if [[ ${backup_count} -gt ${backup_max} ]]; then
        echo "Removing oldest backup file(s)"
        files_to_delete=$(($backup_count - $backup_max))
        #echo $files_to_delete
        to_delete=$(head -n $files_to_delete $history_file)
        for file in $to_delete; do
            echo $file
            rm -fv $file
        done
        sed -i "1,${files_to_delete}d" $history_file
    fi
fi