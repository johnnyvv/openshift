#!/bin/env python3
from socket import gethostname, gethostbyname
from time import sleep
import os
#list of addresses to resolve:
to_lookup = ['google.nl','redhat.com', 'redhat.nl', 'johnnyvanveen.com']
to_sleep = os.getenv('SLEEP',2)

if os.getenv('POD_IP') and os.getenv('POD_NAMESPACE'):
    #we need to replace the ip dots to -
    ip_converted = os.getenv('POD_IP').replace('.','-')
    #format for pod a record is pod ip -.namespace.pod.cluster.local
    to_lookup.append(f"{ip_converted}.{os.getenv('NAMESPACE')}.pod.cluster.local")
if os.getenv('NODE_IP'):
    to_lookup.append(os.getenv('NODE_NAME'))

#get nameserver from /etc/resolv.conf
resolv_file = "/etc/resolv.conf" if os.path.isfile("/etc/resolv.conf") else None
with open(resolv_file,'r') as f:
    resolv_data = f.readlines()

name_srvs = []
search = []

for line in resolv_data:
#remove leading spaces
    line = line.lstrip().strip()
    if not line or line[0] == "#":
        continue         
        #print("Line is a comment")
    if "nameserver" in line[0:10]:
        name_srvs.append(line.strip("nameserver "))
        #print("Line is a nameserver")
    if "search" in line[0:6]:
        for s in line.split(" ",1)[1].strip().rsplit(" "):
            if s:
                search.append(s)

print(name_srvs)
print(search)

while True:
    for domain in to_lookup:
        print(f"querying for domain {domain}")
        print(f"ip address for {domain} - {gethostbyname(domain)}")
        sleep(int(to_sleep))