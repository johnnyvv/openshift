from flask import Flask,request
import subprocess,psutil, os, time, threading

app = Flask(__name__)

def kill_all(process):
    #first kill child
    for child in process.children():
        child.kill()
    #kill process itself
    print(f"Killing main process with pid {process.pid}")
    process.kill()

def start_stress():
    stress_start = subprocess.Popen(["stress", "--cpu", "1"], shell=False)
    print("h")
    stress_proc = psutil.Process(pid=stress_start.pid)
    print("he")
    stress_children = stress_proc.children()
    print("hey")
    
    if len(stress_children) == 1:
        print("heyy")
        for child in stress_children:
            print(f"stress pid {child.pid}")
            print(child.cpu_percent())
            time.sleep(1)
            print(child.cpu_percent())
            time.sleep(5)
            print(child.cpu_percent())
            print(f"Killing child with pid {child.pid}")
            kill_all(stress_proc)
    else:
        print("heyyy")
        kill_all(stress_proc)


@app.route('/')
def index():
    return f"Hello there"

@app.route('/stress')
def stress():
    threading.Thread(target=start_stress).start()
    return "Started stress"

app.run(host="0.0.0.0",port=8080,debug=True)
