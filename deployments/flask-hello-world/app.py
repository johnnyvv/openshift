from flask import Flask
from flask import request

from socket import gethostname

app = Flask(__name__)

@app.route('/')
def index():
    return f"Hello from Flask!\nThis app is running on a pod with name {gethostname()}."

app.run(host="0.0.0.0",port=8080, debug=True)